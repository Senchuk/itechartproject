CREATE TABLE hospital (
   id UUID PRIMARY KEY,
   name VARCHAR (55) NOT NULL,
   address VARCHAR (55) NOT NULL,
   manager_id UUID UNIQUE NOT NULL
);
