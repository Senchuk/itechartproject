INSERT INTO storage (id, hospital_id) VALUES ('14142e8c-b76e-4d0d-8370-8769de25ceb3', null);

INSERT INTO "user" (id, first_name, last_name, email, password, scope, storage_id, created_at)
  VALUES ('feefda0d-01c7-430f-a75a-9dfe5eb2d1e5', 'manager', 'manager', 'main@mail.com',
          '$2a$10$IRWwDqwHpRrp/gn/PJBWT.Ohq8PYQU66OcEdjbCUj2G9dbpdqyMca',
          'ROLE_MAIN_STORAGE_MANAGER', '14142e8c-b76e-4d0d-8370-8769de25ceb3', current_timestamp);
