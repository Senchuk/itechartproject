CREATE TABLE visit (
   id UUID PRIMARY KEY,
   date DATE NOT NULL,
   patient_id UUID NOT NULL,
   medicine_id UUID
);
