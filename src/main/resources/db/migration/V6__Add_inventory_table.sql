CREATE TABLE inventory (
   id UUID PRIMARY KEY,
   storage_id UUID NOT NULL,
   medicine_id UUID NOT NULL,
   amount INTEGER NOT NULL,
   order_id UUID,

CONSTRAINT fk_inventory_medicine
  FOREIGN KEY (medicine_id)
  REFERENCES medicine (id)
);
