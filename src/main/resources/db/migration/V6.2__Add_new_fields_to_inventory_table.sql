ALTER TABLE inventory
ADD COLUMN created_at timestamp NOT NULL;

ALTER TABLE inventory
ADD COLUMN updated_at timestamp;

ALTER TABLE inventory
ADD COLUMN deleted_at timestamp;
