CREATE TYPE order_request_status AS ENUM ('PENDING', 'ACCEPTED', 'REJECTED');

CREATE TABLE order_request (
   id UUID PRIMARY KEY,
   order_id UUID NOT NULL,
   order_status VARCHAR (55) NOT NULL,
   created_at timestamp NOT NULL,
   updated_at timestamp,
   deleted_at timestamp
);

