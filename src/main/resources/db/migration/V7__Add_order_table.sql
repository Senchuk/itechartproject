CREATE TYPE medicine_status AS ENUM ('PENDING', 'WHOLE', 'DAMAGED', 'LOST', 'EXPIRED');

CREATE TABLE "order" (
   id UUID PRIMARY KEY,
   hospital_id UUID NOT NULL,
   medicine_id UUID NOT NULL,
   amount INTEGER NOT NULL,
   medicine_status VARCHAR (55) NOT NULL,
   created_at timestamp NOT NULL,
   updated_at timestamp,
   deleted_at timestamp
);
