CREATE TABLE research (
   id UUID PRIMARY KEY,
   name VARCHAR (55) NOT NULL,
   hospital_id UUID NOT NULL,
   researcher_id UUID NOT NULL,
   created_at timestamp NOT NULL,
   updated_at timestamp,
   deleted_at timestamp
);
