CREATE TYPE scope AS ENUM ('ROLE_SPONSOR', 'ROLE_RESEARCHER', 'ROLE_MAIN_STORAGE_MANAGER', 'ROLE_STORAGE_MANAGER');

CREATE TYPE gender AS ENUM ('MALE', 'FEMALE');

CREATE TYPE patient_status AS ENUM ('SCREENED', 'ACCEPTED_INTO_THE_RESEARCHING', 'RESEARCHING_COMPETED');

CREATE TABLE "user" (
   id UUID PRIMARY KEY,
   first_name VARCHAR (55) NOT NULL,
   last_name VARCHAR (55) NOT NULL,
   email VARCHAR (55) UNIQUE NOT NULL,
   password VARCHAR (255) NOT NULL,
   scope VARCHAR (55) NOT NULL,
   created_at timestamp NOT NULL,
   updated_at timestamp,
   deleted_at timestamp,
   blocked_at timestamp,
   storage_id UUID
);

CREATE TABLE patient (
   id UUID PRIMARY KEY,
   first_name VARCHAR (55) NOT NULL,
   second_name VARCHAR (55) NOT NULL,
   last_name VARCHAR (55) NOT NULL,
   gender VARCHAR (55) NOT NULL,
   dob DATE NOT NULL,
   status VARCHAR (55) NOT NULL,
   created_at timestamp NOT NULL,
   updated_at timestamp,
   deleted_at timestamp,
   user_id UUID NOT NULL
);
