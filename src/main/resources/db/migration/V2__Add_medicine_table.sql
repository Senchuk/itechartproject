CREATE TYPE medicine_type AS ENUM ('TABLET', 'CAPSULE', 'DROPS', 'MIXTURE', 'OINTMENT');

CREATE TABLE medicine (
   id UUID PRIMARY KEY,
   name VARCHAR (55) NOT NULL,
   producer VARCHAR (55) NOT NULL,
   description VARCHAR (256) NOT NULL,
   type VARCHAR (55) NOT NULL,
   expiration_date DATE NOT NULL,
   created_at timestamp NOT NULL,
   updated_at timestamp,
   deleted_at timestamp
);
