package com.itechart.userservice.medicine.api;

import static com.itechart.userservice.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.api.MedicinesApi;
import com.itechart.userservice.generated.model.Medicine;
import com.itechart.userservice.medicine.service.MedicineService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class MedicinesApiImpl implements MedicinesApi {

    private final MedicineService medicineService;

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER')")
    public ResponseEntity<Medicine> createMedicine(Medicine medicine) {
        var medicineResponse = medicineService.create(medicine);
        return new ResponseEntity<>(medicineResponse, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER') or hasRole('ROLE_STORAGE_MANAGER')")
    public ResponseEntity<List<Medicine>> getAllMedicine(Integer page, Integer size, List<String> sort,
                                                         List<String> filter, String search) {
        var medicines = medicineService.getPage(page, size, sort, filter, search);
        var headers = generatePaginationHeaders(medicines);
        return new ResponseEntity<>(medicines, headers, OK);
    }

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER') or hasRole('ROLE_STORAGE_MANAGER')")
    public ResponseEntity<Medicine> getMedicineById(UUID id) {
        var medicineResponse = medicineService.getById(id);
        return new ResponseEntity<>(medicineResponse, OK);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Medicine> getRandomMedicine() {
        var medicineResponse = medicineService.getRandom();
        return new ResponseEntity<>(medicineResponse, OK);
    }

}
