package com.itechart.userservice.medicine.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.medicine.entity.MedicineEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicineRepository extends JpaRepository<MedicineEntity, UUID> {

    @Query("SELECT medicine " +
           "FROM MedicineEntity AS medicine " +
           "WHERE medicine.id = :id " +
           " AND medicine.deletedAt IS NULL")
    Optional<MedicineEntity> findById(@Param("id") UUID id);

    @Query("SELECT medicine " +
           "FROM MedicineEntity AS medicine " +
           "WHERE medicine.id IN (:ids)")
    List<MedicineEntity> findAllById(@Param("ids") List<UUID> ids);

    @Query("SELECT medicine " +
           "FROM MedicineEntity AS medicine " +
           "WHERE medicine.deletedAt = CURRENT_DATE")
    List<MedicineEntity> findAllExpiredToday();

}
