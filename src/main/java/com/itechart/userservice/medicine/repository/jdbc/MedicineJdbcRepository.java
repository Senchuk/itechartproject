package com.itechart.userservice.medicine.repository.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MedicineJdbcRepository {

    private static final String UPDATE_MEDICINE =
            "UPDATE Medicine SET deleted_at = CURRENT_TIMESTAMP " +
                    "WHERE deleted_at IS NULL AND expiration_date <= CURRENT_DATE";

    private final JdbcTemplate jdbcTemplate;

    public void deleteExpired() {
        jdbcTemplate.update(UPDATE_MEDICINE);
    }

}
