package com.itechart.userservice.medicine.repository.elastic;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.medicine.document.MedicineDocument;
import com.itechart.userservice.medicine.document.enums.MedicineTypeDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicineElasticRepository extends ElasticsearchRepository<MedicineDocument, UUID> {

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"match\" : { \"type\" : \"?0\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"name\" : \"?1\"}}," +
           "       { \"match\" : { \"producer\" : \"?1\"}}," +
           "       { \"match_phrase\" : { \"description\" : \"?1\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<MedicineDocument> findAll(List<MedicineTypeDocument> filter,
                                   String search,
                                   Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"should\" : [" +
           "    { \"match\" : { \"name\" : \"?0\"}}," +
           "    { \"match\" : { \"producer\" : \"?0\"}}," +
           "    { \"match_phrase\" : { \"description\" : \"?0\"}}" +
           "  ]" +
           " }" +
           "}")
    Page<MedicineDocument> findAll(String search,
                                   Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : " +
           "    { \"match\" : { \"type\" : \"?0\"}}" +
           " }" +
           "}")
    Page<MedicineDocument> findAll(List<MedicineTypeDocument> filter,
                                   Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}" +
           " }" +
           "}")
    Page<MedicineDocument> findAll(Pageable pageable);

}
