package com.itechart.userservice.medicine.mapper;

import com.itechart.userservice.generated.model.Medicine;
import com.itechart.userservice.medicine.document.MedicineDocument;
import com.itechart.userservice.medicine.entity.MedicineEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MedicineMapper {

    MedicineMapper MEDICINE_MAPPER = Mappers.getMapper(MedicineMapper.class);

    MedicineEntity toEntity(Medicine medicine);

    Medicine toModel(MedicineEntity medicineEntity);

    MedicineDocument toDocument(MedicineEntity medicineEntity);

}
