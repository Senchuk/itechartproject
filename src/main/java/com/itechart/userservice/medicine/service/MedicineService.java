package com.itechart.userservice.medicine.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.Medicine;

public interface MedicineService {

    Medicine create(Medicine medicine);

    Medicine getById(UUID id);

    List<Medicine> getPage(Integer page, Integer size, List<String> sort,
                           List<String> filter, String search);

    List<Medicine> getAll();

    Medicine getRandom();

}
