package com.itechart.userservice.medicine.service.elastic;

import static com.itechart.userservice.utils.ExceptionUtils.elasticsearchQueryException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.medicine.entity.MedicineEntity;
import com.itechart.userservice.medicine.repository.MedicineRepository;
import com.itechart.userservice.medicine.service.MedicineElasticService;

import org.elasticsearch.client.Client;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MedicineElasticServiceImpl implements MedicineElasticService {

    private final static String MEDICINE_INDEX = "medicine";
    private final static String MEDICINE_TYPE = "_doc";

    private final MedicineRepository medicineRepository;

    private final ApplicationContext applicationContext;

    public void deleteExpired() {
        var expiredMedicines = medicineRepository.findAllExpiredToday();
        if(expiredMedicines.isEmpty()) {
            return;
        }
        var medicineIds = expiredMedicines.stream().map(MedicineEntity::getId).collect(Collectors.toList());
        softDelete(medicineIds);
    }

    private void softDelete(List<UUID> medicineIds) {
        var client = applicationContext.getBean(Client.class);
        var bulkRequest = client.prepareBulk();
        for (UUID id : medicineIds) {
            try {
                bulkRequest.add(client.prepareUpdate(MEDICINE_INDEX, MEDICINE_TYPE, id.toString())
                        .setDoc(jsonBuilder()
                                .startObject()
                                .field("deletedAt", Date.from(Instant.now()))
                                .endObject()));
            } catch (IOException e) {
                throw elasticsearchQueryException("Cannot add object to elasticsearch query");
            }
        }
        var bulkResponse = bulkRequest.get();
        if (bulkResponse.hasFailures()) {
            throw elasticsearchQueryException("Elasticsearch query error");
        }
    }

}
