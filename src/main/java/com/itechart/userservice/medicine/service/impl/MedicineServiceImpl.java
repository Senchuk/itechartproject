package com.itechart.userservice.medicine.service.impl;

import static java.util.Collections.emptyList;

import static com.itechart.userservice.medicine.mapper.MedicineMapper.MEDICINE_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.incorrectDataException;
import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;
import static com.itechart.userservice.utils.FilterUtils.convertToMedicineTypeDocument;
import static com.itechart.userservice.utils.PagingUtils.toPageable;
import static com.itechart.userservice.utils.SecurityUtils.getCurrentUser;
import static com.itechart.userservice.utils.SortUtils.parseSortOrDefault;

import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.Medicine;
import com.itechart.userservice.inventory.repository.jdbc.InventoryJdbcRepository;
import com.itechart.userservice.medicine.document.MedicineDocument;
import com.itechart.userservice.medicine.document.enums.MedicineTypeDocument;
import com.itechart.userservice.medicine.repository.MedicineRepository;
import com.itechart.userservice.medicine.repository.elastic.MedicineElasticRepository;
import com.itechart.userservice.medicine.service.MedicineService;
import com.itechart.userservice.research.service.ResearchService;
import com.itechart.userservice.storage.service.StorageService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MedicineServiceImpl implements MedicineService {

    private final ResearchService researchService;
    private final StorageService storageService;

    private final MedicineRepository medicineRepository;
    private final MedicineElasticRepository medicineElasticRepository;

    private final InventoryJdbcRepository inventoryJdbcRepository;

    @Override
    @Transactional
    public Medicine create(Medicine medicine) {
        var now = Date.from(Instant.now());
        if(medicine.getExpirationDate().equals(now) || medicine.getExpirationDate().before(now)) {
            throw incorrectDataException("Medicine expired or expire today");
        }
        var medicineEntity = MEDICINE_MAPPER.toEntity(medicine);
        var savedMedicine = medicineRepository.save(medicineEntity);
        var medicineDocument = MEDICINE_MAPPER.toDocument(savedMedicine);
        medicineElasticRepository.save(medicineDocument);
        return MEDICINE_MAPPER.toModel(savedMedicine);
    }

    @Override
    @Transactional(readOnly = true)
    public Medicine getById(UUID id) {
        var medicineEntity = medicineRepository.findById(id).orElseThrow(() ->
                notFoundEntity(String.format("Medicine with such id %s was not found", id)));
        return MEDICINE_MAPPER.toModel(medicineEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Medicine> getPage(Integer page, Integer size, List<String> sort,
                                  List<String> filterType, String search) {
        var sorting = parseSortOrDefault(sort, MedicineDocument.class);
        var pageable = toPageable(page, size, sorting);
        var filter = convertToMedicineTypeDocument(filterType);
        var medicineDocuments = getDocumentsPage(filter, search, pageable);
        if(medicineDocuments.isEmpty()) {
            return emptyList();
        }
        var ids = medicineDocuments.stream().map(MedicineDocument::getId).collect(Collectors.toList());
        var medicines = medicineRepository.findAllById(ids);
        return medicines.stream().map(MEDICINE_MAPPER::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Medicine> getAll() {
        return medicineRepository.findAll().stream().map(MEDICINE_MAPPER::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Medicine getRandom() {
        var researcherId = getCurrentUser().getId();
        var hospital = researchService.getByResearcherId(researcherId);
        var storageId = storageService.getByHospitalId(hospital.getHospitalId()).getId();
        var medicineId = inventoryJdbcRepository.getRandomMedicineByStorageId(storageId);
        return getById(medicineId);
    }

    private Page<MedicineDocument> getDocumentsPage(List<MedicineTypeDocument> filter, String search, Pageable pageable) {
        if (filter == null && search ==null) {
            return medicineElasticRepository.findAll(pageable);
        } else if(filter == null) {
            return medicineElasticRepository.findAll(search, pageable);
        } else if(search == null) {
            return medicineElasticRepository.findAll(filter, pageable);
        } else {
            return medicineElasticRepository.findAll(filter, search, pageable);
        }
    }

}
