package com.itechart.userservice.medicine.entity;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

import java.util.Date;
import java.util.UUID;
import javax.persistence.*;

import com.itechart.userservice.common.entity.AbstractEntity;
import com.itechart.userservice.medicine.entity.enums.MedicineTypeEntity;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medicine")
@EqualsAndHashCode(callSuper = true)
public class MedicineEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "producer", nullable = false)
    private String producer;

    @Column(name = "description", nullable = false)
    private String description;

    @Enumerated(STRING)
    @Column(name = "type", nullable = false)
    private MedicineTypeEntity type;

    @DateTimeFormat
    @Column(name = "expiration_date", nullable = false)
    private Date expirationDate;

}
