package com.itechart.userservice.medicine.entity.enums;

import javax.persistence.Table;

import lombok.Getter;

@Getter
@Table(name = "medicine_type")
public enum MedicineTypeEntity {

    TABLET,
    CAPSULE,
    DROPS,
    MIXTURE,
    OINTMENT

}
