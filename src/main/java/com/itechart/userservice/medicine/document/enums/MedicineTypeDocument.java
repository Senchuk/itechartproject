package com.itechart.userservice.medicine.document.enums;

public enum MedicineTypeDocument {

    TABLET,
    CAPSULE,
    DROPS,
    MIXTURE,
    OINTMENT

}
