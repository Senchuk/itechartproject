package com.itechart.userservice.medicine.document;

import static javax.persistence.EnumType.STRING;

import static org.springframework.data.elasticsearch.annotations.FieldType.Text;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.itechart.userservice.medicine.document.enums.MedicineTypeDocument;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import lombok.Data;

@Data
@Document(indexName = "medicine", type = "_doc")
public class MedicineDocument{

    @Id
    private UUID id;

    @Field(type = Text, fielddata = true)
    private String name;

    private String producer;

    private String description;

    @Enumerated(STRING)
    private MedicineTypeDocument type;

    private Date expirationDate;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

}
