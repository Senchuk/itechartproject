package com.itechart.userservice.statistic.service;

import com.itechart.userservice.generated.model.Statistics;

public interface StatisticsService {

    Statistics collect();

}
