package com.itechart.userservice.statistic.service.impl;

import com.itechart.userservice.generated.model.Statistics;
import com.itechart.userservice.patient.repository.PatientRepository;
import com.itechart.userservice.statistic.service.StatisticsService;
import com.itechart.userservice.visit.repository.VisitRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {

    private final PatientRepository patientRepository;
    private final VisitRepository visitRepository;

    @Override
    @Transactional(readOnly = true)
    public Statistics collect() {
        var patientQuantity = patientRepository.countPatients();
        var visitQuantity = visitRepository.countTotalVisitQuantity();
        var distributedMedicineQuantity = visitRepository.countTotalDistributedMedicineQuantity();
        var statistics = new Statistics();
        statistics.setPatientQuantity(patientQuantity);
        statistics.setDistributedMedicineQuantity(distributedMedicineQuantity);
        statistics.setVisitQuantity(visitQuantity);
        return statistics;
    }

}
