package com.itechart.userservice.statistic.api;

import static org.springframework.http.HttpStatus.OK;

import com.itechart.userservice.generated.api.StatisticsApi;
import com.itechart.userservice.generated.model.Statistics;
import com.itechart.userservice.statistic.service.StatisticsService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class StatisticsApiImpl implements StatisticsApi {

    private final StatisticsService statisticsService;

    @PreAuthorize("hasRole('ROLE_RESEARCHER') or hasRole('ROLE_SPONSOR')")
    public ResponseEntity<Statistics> collectStatistics(){
        var statistics = statisticsService.collect();
        return new ResponseEntity<>(statistics, OK);
    }

}
