package com.itechart.userservice.authentication.api;

import static org.springframework.http.HttpStatus.CREATED;

import com.itechart.userservice.authentication.service.AuthService;
import com.itechart.userservice.generated.api.AuthApi;
import com.itechart.userservice.generated.model.SignInRequest;
import com.itechart.userservice.generated.model.SignUpRequest;
import com.itechart.userservice.generated.model.TokenResponse;
import com.itechart.userservice.generated.model.User;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class AuthApiImpl implements AuthApi {

    private final AuthService authService;

    public ResponseEntity<TokenResponse> signIn(SignInRequest signInRequest) {
        var response = authService.signIn(signInRequest);
        return new ResponseEntity<>(response, CREATED);
    }

    public ResponseEntity<User> signUp(SignUpRequest signUpRequest) {
        var response = authService.signUp(signUpRequest);
        return new ResponseEntity<>(response, CREATED);
    }

    public ResponseEntity<TokenResponse> refreshToken(String refreshToken) {
        var response = authService.refresh(refreshToken);
        return new ResponseEntity<>(response, CREATED);
    }

}
