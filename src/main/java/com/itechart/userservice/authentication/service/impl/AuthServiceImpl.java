package com.itechart.userservice.authentication.service.impl;

import static com.itechart.userservice.user.entity.enums.UserScopeEntity.ROLE_MAIN_STORAGE_MANAGER;
import static com.itechart.userservice.user.mapper.UserMapper.USER_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.*;

import java.util.Collections;

import com.itechart.userservice.authentication.service.AuthService;
import com.itechart.userservice.generated.model.SignInRequest;
import com.itechart.userservice.generated.model.SignUpRequest;
import com.itechart.userservice.generated.model.TokenResponse;
import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.security.JwtTokenProvider;
import com.itechart.userservice.user.repository.UserRepository;
import com.itechart.userservice.user.repository.elastic.UserElasticRepository;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final UserElasticRepository userElasticRepository;

    private final AuthenticationManager authenticationManager;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    @Transactional(readOnly = true)
    public TokenResponse signIn(SignInRequest signInRequest) {
        var user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow(() ->
                notFoundEntity(String.format("User with such username %s was not found", signInRequest.getEmail()))
        );
        if (user.getBlockedAt() != null) {
            throw userWasBlockedException(String.format("User with such id %s was blocked", user.getId()));
        }
        var authenticationToken = new UsernamePasswordAuthenticationToken(
                user.getEmail(), signInRequest.getPassword()
        );
        try {
            authenticationManager.authenticate(authenticationToken);
        } catch (Exception e) {
            throw notFoundEntity("Password is not correct");
        }
        return jwtTokenProvider.createTokens(user.getEmail(), Collections.singletonList(user.getScope().toString()));
    }

    @Override
    @Transactional
    public User signUp(SignUpRequest signUpRequest) throws IllegalStateException {
        var userEntity = USER_MAPPER.toEntity(signUpRequest);
        if (userEntity.getScope().equals(ROLE_MAIN_STORAGE_MANAGER)){
            throw incorrectDataException("User cannot be registered as Main storage manager");
        }
        userRepository.findByEmail(userEntity.getEmail()).map(exception -> {
            throw userAlreadyRegistered(
                    String.format("User with email %s has been already registered", userEntity.getEmail()));
        });
        var encodedPassword = bCryptPasswordEncoder.encode(signUpRequest.getPassword());
        userEntity.setPassword(encodedPassword);
        var savedUser = userRepository.save(userEntity);
        var userDocument = USER_MAPPER.toDocument(savedUser);
        userElasticRepository.save(userDocument);
        return USER_MAPPER.toShortModel(savedUser);
    }

    @Override
    public TokenResponse refresh(String refreshToken) {
        var isValid = jwtTokenProvider.validateToken(refreshToken);
        if (isValid) {
            var username = jwtTokenProvider.getUsername(refreshToken);
            var scope = jwtTokenProvider.getScope(refreshToken);
            var listOfScope = Collections.singletonList(scope);
            return jwtTokenProvider.createTokens(username, listOfScope);
        } else {
            throw invalidAuthException("Invalid or expired authentication");
        }
    }

}
