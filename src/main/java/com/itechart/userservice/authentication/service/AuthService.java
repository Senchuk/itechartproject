package com.itechart.userservice.authentication.service;

import com.itechart.userservice.generated.model.SignInRequest;
import com.itechart.userservice.generated.model.SignUpRequest;
import com.itechart.userservice.generated.model.TokenResponse;
import com.itechart.userservice.generated.model.User;

public interface AuthService {

    TokenResponse signIn(SignInRequest signInRequest);

    User signUp(SignUpRequest signUpRequest);

    TokenResponse refresh(String refreshToken);

}
