package com.itechart.userservice.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class NoSuchFieldException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public NoSuchFieldException(String message) {
        super(message);
        status = NOT_FOUND;
    }

}
