package com.itechart.userservice.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class EntityNotFoundException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public EntityNotFoundException(String message) {
        super(message);
        status = NOT_FOUND;
    }

}
