package com.itechart.userservice.exception;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class ElasticsearchQueryException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public ElasticsearchQueryException(String message) {
        super(message);
        status = INTERNAL_SERVER_ERROR;
    }

}
