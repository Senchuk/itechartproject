package com.itechart.userservice.exception;

import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandling extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserAlreadyRegisteredException.class)
    public ResponseEntity<Object> notFoundExceptionHandler(UserAlreadyRegisteredException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> notFoundExceptionHandler(EntityNotFoundException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(IncorrectDataException.class)
    public ResponseEntity<Object> notFoundExceptionHandler(IncorrectDataException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(NoSuchFieldException.class)
    public Object notFoundExceptionHandler(NoSuchFieldException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(IncorrectRequestException.class)
    public Object notFoundExceptionHandler(IncorrectRequestException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(InvalidAuthException.class)
    public Object notFoundExceptionHandler(InvalidAuthException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(UserWasBlockedException.class)
    public Object notFoundExceptionHandler(UserWasBlockedException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(ElasticsearchQueryException.class)
    public Object notFoundExceptionHandler(ElasticsearchQueryException e, WebRequest request) {
        var error = new UnexpectedError();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ResponseBody
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        var errors = ex.getBindingResult().getFieldErrors().stream()
                .map(error ->  {
                    var unexpectedError = new UnexpectedError();
                    unexpectedError.setMessage(error.getDefaultMessage());
                    return unexpectedError;
                })
                .collect(Collectors.toList());
        return new ResponseEntity<>(errors, headers, status);
    }

}
