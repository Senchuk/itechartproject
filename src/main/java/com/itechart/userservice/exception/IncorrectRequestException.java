package com.itechart.userservice.exception;

import static org.springframework.http.HttpStatus.FORBIDDEN;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class IncorrectRequestException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public IncorrectRequestException(String message) {
        super(message);
        status = FORBIDDEN;
    }

}
