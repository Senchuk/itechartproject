package com.itechart.userservice.exception;

import lombok.Data;

@Data
public class UnexpectedError {

    private String message;

}
