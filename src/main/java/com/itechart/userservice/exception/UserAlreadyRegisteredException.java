package com.itechart.userservice.exception;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class UserAlreadyRegisteredException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public UserAlreadyRegisteredException(String message) {
        super(message);
        status = UNPROCESSABLE_ENTITY;
    }

}
