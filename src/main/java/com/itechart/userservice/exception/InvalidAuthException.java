package com.itechart.userservice.exception;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class InvalidAuthException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public InvalidAuthException(String message) {
        super(message);
        status = SERVICE_UNAVAILABLE;
    }

}
