package com.itechart.userservice.filter;

import static javax.servlet.http.HttpServletResponse.SC_OK;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(HIGHEST_PRECEDENCE)
class CorsFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        var response = (HttpServletResponse) resp;
        var request = (HttpServletRequest) req;
        response.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers",
                "x-requested-with, authorization, Content-Type, " +
                        "Authorization, credential, X-XSRF-TOKEN, X-Total-Count"
        );
        response.setHeader("Access-Control-Expose-Headers", "X-Total-Count, X-Paging-PageSize");

        if (Objects.equals("OPTIONS".toLowerCase(), request.getMethod().toLowerCase())) {
            response.setStatus(SC_OK);
        } else {
            chain.doFilter(req, resp);
        }
    }

}
