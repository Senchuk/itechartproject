package com.itechart.userservice.patient.api;

import static com.itechart.userservice.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.api.PatientsApi;
import com.itechart.userservice.generated.model.ChangeStatusRequest;
import com.itechart.userservice.generated.model.Patient;
import com.itechart.userservice.patient.service.PatientService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class PatientsApiImpl implements PatientsApi {

    private final PatientService patientService;

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Patient> getPatientById(UUID id){
        var patient = patientService.getById(id);
        return new ResponseEntity<>(patient, OK);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Patient> createPatient(Patient patient) {
        var patientResponse = patientService.create(patient);
        return new ResponseEntity<>(patientResponse, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<List<Patient>> getAllPatients(Integer page, Integer size, List<String> sort,
                                                        List<String> statuses, String search) {
        var patients = patientService.getPage(page, size, sort, statuses, search);
        var headers = generatePaginationHeaders(patients);
        return new ResponseEntity<>(patients, headers, OK);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Patient> updatePatientStatus(UUID id, ChangeStatusRequest changeStatusRequest) {
        var patient = patientService.updateStatus(id, changeStatusRequest);
        return new ResponseEntity<>(patient, OK);
    }

}
