package com.itechart.userservice.patient.document.enums;

public enum PatientStatusDocument {

    SCREENED,
    ACCEPTED_INTO_THE_RESEARCHING,
    RESEARCH_COMPLETED

}
