package com.itechart.userservice.patient.document.enums;

public enum PatientGenderDocument {

    MALE,
    FEMALE

}
