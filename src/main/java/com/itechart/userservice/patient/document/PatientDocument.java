package com.itechart.userservice.patient.document;

import static javax.persistence.EnumType.STRING;

import static org.springframework.data.elasticsearch.annotations.FieldType.Text;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.itechart.userservice.patient.document.enums.PatientGenderDocument;
import com.itechart.userservice.patient.document.enums.PatientStatusDocument;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Document(indexName = "patient", type = "_doc")
public class PatientDocument {

    @Id
    private UUID id;

    @Field(type = Text, fielddata = true)
    private String firstName;

    @Field(type = Text, fielddata = true)
    private String secondName;

    @Field(type = Text, fielddata = true)
    private String lastName;

    @Enumerated(STRING)
    private PatientGenderDocument gender;

    @DateTimeFormat
    private Date dob;

    @Enumerated(STRING)
    private PatientStatusDocument status;

    private UUID researcherId;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

}
