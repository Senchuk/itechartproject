package com.itechart.userservice.patient.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.ChangeStatusRequest;
import com.itechart.userservice.generated.model.Patient;

public interface PatientService {

    Patient create(Patient patient);

    Patient getById(UUID id);

    List<Patient> getPage(Integer page, Integer size, List<String> sort,  List<String> statuses, String search);

    Patient updateStatus(UUID id, ChangeStatusRequest changeStatusRequest);

}
