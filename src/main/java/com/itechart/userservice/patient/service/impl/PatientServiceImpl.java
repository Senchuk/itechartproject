package com.itechart.userservice.patient.service.impl;

import static java.util.Collections.emptyList;

import static com.itechart.userservice.generated.model.PatientStatus.RESEARCH_COMPLETED;
import static com.itechart.userservice.patient.mapper.PatientMapper.PATIENT_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;
import static com.itechart.userservice.utils.FilterUtils.convertToPatientStatusDocument;
import static com.itechart.userservice.utils.PagingUtils.toPageable;
import static com.itechart.userservice.utils.SecurityUtils.getCurrentUser;
import static com.itechart.userservice.utils.SortUtils.parseSortOrDefault;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.ChangeStatusRequest;
import com.itechart.userservice.generated.model.Patient;
import com.itechart.userservice.patient.document.PatientDocument;
import com.itechart.userservice.patient.document.enums.PatientStatusDocument;
import com.itechart.userservice.patient.entity.PatientEntity;
import com.itechart.userservice.patient.repository.PatientRepository;
import com.itechart.userservice.patient.repository.elastic.PatientElasticRepository;
import com.itechart.userservice.patient.service.PatientService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    private final PatientElasticRepository patientElasticRepository;

    @Override
    @Transactional
    public Patient create(Patient patient) {
        var id = getCurrentUser().getId();
        var patientEntity = PATIENT_MAPPER.toEntity(patient);
        patientEntity.setResearcherId(id);
        var savedPatient = patientRepository.save(patientEntity);
        var patientDocument = PATIENT_MAPPER.toDocument(savedPatient);
        patientElasticRepository.save(patientDocument);
        return PATIENT_MAPPER.toModel(savedPatient);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Patient> getPage(Integer page, Integer size, List<String> sort, List<String> statuses, String search) {
        var researcherId = getCurrentUser().getId();
        var sorting = parseSortOrDefault(sort, PatientDocument.class);
        var pageable = toPageable(page, size, sorting);
        var filter = convertToPatientStatusDocument(statuses);
        var patientDocuments = getDocumentsPage(researcherId, filter, search, pageable);
        if(patientDocuments.isEmpty()) {
            return emptyList();
        }
        var ids = patientDocuments.stream().map(PatientDocument::getId).collect(Collectors.toList());
        var patients = patientRepository.findAllById(ids);
        return patients.stream().map(PATIENT_MAPPER::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Patient updateStatus(UUID id, ChangeStatusRequest changeStatusRequest) {
        var patient = getById(id);
        patient.setStatus(changeStatusRequest.getStatus());
        if (changeStatusRequest.getStatus().equals(RESEARCH_COMPLETED)) {
            patient.setDeletedAt(Instant.now());
        }
        var patientToUpdate = PATIENT_MAPPER.toEntity(patient);
        var savedPatient = patientRepository.save(patientToUpdate);
        var patientDocument = PATIENT_MAPPER.toDocument(savedPatient);
        patientElasticRepository.save(patientDocument);
        return PATIENT_MAPPER.toModel(savedPatient);
    }

    @Override
    @Transactional(readOnly = true)
    public Patient getById(UUID id) {
        var patient = getByIdOrThrow(id);
        return PATIENT_MAPPER.toModel(patient);
    }

    private PatientEntity getByIdOrThrow(UUID id) {
        return patientRepository.findById(id).orElseThrow(() ->
                notFoundEntity(String.format("Patient with such %s id was not found", id)));
    }

    private Page<PatientDocument> getDocumentsPage(UUID researcherId, List<PatientStatusDocument> filter,
                                                   String search, Pageable pageable) {
        if(filter == null & search == null) {
            return patientElasticRepository.findAll(researcherId, pageable);
        } else if (filter == null) {
            return patientElasticRepository.findAll(researcherId, search, pageable);
        } else if(search == null) {
            return patientElasticRepository.findAll(researcherId, filter, pageable);
        } else {
            return patientElasticRepository.findAll(researcherId, filter, search, pageable);
        }
    }

}
