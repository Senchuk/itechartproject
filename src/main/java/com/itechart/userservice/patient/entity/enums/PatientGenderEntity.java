package com.itechart.userservice.patient.entity.enums;

import javax.persistence.Table;

import lombok.Getter;

@Getter
@Table(name = "gender")
public enum PatientGenderEntity {

    MALE,
    FEMALE

}
