package com.itechart.userservice.patient.entity;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

import java.util.Date;
import java.util.UUID;
import javax.persistence.*;

import com.itechart.userservice.common.entity.AbstractEntity;
import com.itechart.userservice.patient.entity.enums.PatientGenderEntity;
import com.itechart.userservice.patient.entity.enums.PatientStatusEntity;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "patient")
@EqualsAndHashCode(callSuper = true)
public class PatientEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "second_name", nullable = false)
    private String secondName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Enumerated(STRING)
    @Column(name = "gender", nullable = false)
    private PatientGenderEntity gender;

    @DateTimeFormat
    @Column(name = "dob", nullable = false)
    private Date dob;

    @Enumerated(STRING)
    @Column(name = "status", nullable = false)
    private PatientStatusEntity status;

    @Column(name = "researcher_id")
    private UUID researcherId;

}
