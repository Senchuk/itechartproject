package com.itechart.userservice.patient.entity.enums;

import javax.persistence.Table;

import lombok.Getter;

@Getter
@Table(name = "status")
public enum PatientStatusEntity {

    SCREENED,
    ACCEPTED_INTO_THE_RESEARCHING,
    RESEARCH_COMPLETED

}
