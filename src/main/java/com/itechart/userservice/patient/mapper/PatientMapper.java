package com.itechart.userservice.patient.mapper;

import com.itechart.userservice.generated.model.Patient;
import com.itechart.userservice.patient.document.PatientDocument;
import com.itechart.userservice.patient.entity.PatientEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PatientMapper {

    PatientMapper PATIENT_MAPPER = Mappers.getMapper(PatientMapper.class);

    PatientEntity toEntity(Patient patient);

    Patient toModel(PatientEntity patientEntity);

    PatientDocument toDocument(PatientEntity patientEntity);

}
 
