package com.itechart.userservice.patient.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.patient.entity.PatientEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends JpaRepository<PatientEntity, UUID> {

    @Query("SELECT patient " +
           "FROM PatientEntity AS patient " +
           "WHERE patient.id = :id " +
           " AND patient.deletedAt IS NULL")
    Optional<PatientEntity> findById(@Param("id") UUID id);


    @Query("SELECT patient " +
           "FROM PatientEntity AS patient " +
           "WHERE patient.id IN (:ids)")
    List<PatientEntity> findAllById(@Param("ids") List<UUID> ids);

    @Query("SELECT COUNT(patient) " +
           "FROM PatientEntity AS patient " +
           "WHERE patient.deletedAt IS NULL")
    Integer countPatients();

}
