package com.itechart.userservice.patient.repository.elastic;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.patient.document.PatientDocument;
import com.itechart.userservice.patient.document.enums.PatientStatusDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientElasticRepository extends ElasticsearchRepository<PatientDocument, UUID> {

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"match\" : { \"researcherId\" : \"?0\"}}," +
           "    { \"match\" : { \"status\" : \"?1\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"firstName\" : \"?2\"}}," +
           "       { \"match\" : { \"lastName\" : \"?2\"}}," +
           "       { \"match\" : { \"secondName\" : \"?2\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<PatientDocument> findAll(UUID researcherId,
                                  List<PatientStatusDocument> statuses,
                                  String search,
                                  Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"match\" : { \"researcherId\" : \"?0\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"firstName\" : \"?1\"}}," +
           "       { \"match\" : { \"lastName\" : \"?1\"}}," +
           "       { \"match\" : { \"secondName\" : \"?1\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<PatientDocument> findAll(UUID researcherId,
                                  String search,
                                  Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"match\" : { \"researcherId\" : \"?0\"}}," +
           "    { \"match\" : { \"status\" : \"?1\"}}" +
           "  ]" +
           " }" +
           "}")
    Page<PatientDocument> findAll(UUID researcherId,
                                  List<PatientStatusDocument> statuses,
                                  Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : " +
           "    { \"match\" : { \"researcherId\" : \"?0\"}}" +
           " }" +
           "}")
    Page<PatientDocument> findAll(UUID researcherId,
                                  Pageable pageable);

}
