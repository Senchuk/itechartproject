package com.itechart.userservice.user.entity;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

import java.time.Instant;
import java.util.UUID;
import javax.persistence.*;

import com.itechart.userservice.common.entity.AbstractEntity;
import com.itechart.userservice.user.entity.enums.UserScopeEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`user`")
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Enumerated(STRING)
    @Column(name = "scope", nullable = false)
    private UserScopeEntity scope;

    @Column(name = "blocked_at")
    private Instant blockedAt;

    @Column(name = "storage_id")
    private UUID storageId;

}
