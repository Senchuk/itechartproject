package com.itechart.userservice.user.entity.enums;

import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Table(name = "scope")
public enum UserScopeEntity {

    ROLE_SPONSOR,
    ROLE_RESEARCHER,
    ROLE_MAIN_STORAGE_MANAGER,
    ROLE_STORAGE_MANAGER

}
