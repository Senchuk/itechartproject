package com.itechart.userservice.user.service.impl;

import static java.util.Collections.emptyList;

import static com.itechart.userservice.user.entity.enums.UserScopeEntity.*;
import static com.itechart.userservice.user.mapper.UserMapper.USER_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.*;
import static com.itechart.userservice.utils.FilterUtils.convertToUserScopeDocument;
import static com.itechart.userservice.utils.PagingUtils.toPageable;
import static com.itechart.userservice.utils.SecurityUtils.getCurrentUser;
import static com.itechart.userservice.utils.SortUtils.parseSortOrDefault;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.ChangePasswordRequest;
import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.generated.model.UserScope;
import com.itechart.userservice.hospital.repository.HospitalRepository;
import com.itechart.userservice.research.repository.ResearchRepository;
import com.itechart.userservice.user.document.UserDocument;
import com.itechart.userservice.user.document.enums.UserScopeDoc;
import com.itechart.userservice.user.entity.UserEntity;
import com.itechart.userservice.user.repository.UserRepository;
import com.itechart.userservice.user.repository.elastic.UserElasticRepository;
import com.itechart.userservice.user.service.UserService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final HospitalRepository hospitalRepository;
    private final UserRepository userRepository;
    private final UserElasticRepository userElasticRepository;
    private final ResearchRepository researchRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User getCurrent() {
        var id = getCurrentUser().getId();
        return getById(id);
    }

    @Override
    @Transactional
    public User update(User user) {
        var userEntity =  getUserEntityById(user.getId());
        var userToSave = USER_MAPPER.toEntity(userEntity, user);
        var updatedUser = userRepository.save(userToSave);
        var userDocument = USER_MAPPER.toDocument(updatedUser);
        userElasticRepository.save(userDocument);
        return USER_MAPPER.toShortModel(updatedUser);
    }

    @Override
    @Transactional
    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        var id = getCurrentUser().getId();
        var userEntity = getUserEntityById(id);
        var matches = bCryptPasswordEncoder
                .matches(changePasswordRequest.getOldPassword(), userEntity.getPassword());
        if (matches) {
            var encodedPassword = bCryptPasswordEncoder.encode(changePasswordRequest.getNewPassword());
            userEntity.setPassword(encodedPassword);
            var savedUser = userRepository.save(userEntity);
            var userDocument = USER_MAPPER.toDocument(savedUser);
            userElasticRepository.save(userDocument);
        } else {
            throw incorrectDataException("Password is not correct");
        }
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        var userEntityForDelete = getUserEntityById(id);
        if (userEntityForDelete.getScope().equals(ROLE_SPONSOR) ||
                userEntityForDelete.getScope().equals(ROLE_MAIN_STORAGE_MANAGER)) {
            throw incorrectRequestException(
                    String.format("User with role %s cannot be deleted", userEntityForDelete.getScope()));
        }
        if (userEntityForDelete.getScope().equals(ROLE_STORAGE_MANAGER) && hospitalRepository.existsByManagerId(id)) {
                throw incorrectDataException(String.format(
                        "Storage manager with such id %s cannot be deleted, because he is attached to the hospital", id));
        }
        if (userEntityForDelete.getScope().equals(ROLE_RESEARCHER) && researchRepository.existsByResearcherId(id)) {
                throw incorrectDataException(String.format(
                        "Researcher with such id %s cannot be deleted, because he is doing research", id));
        }
        userEntityForDelete.setDeletedAt(Instant.now());
        var deleteUser = userRepository.save(userEntityForDelete);
        var userDocument = USER_MAPPER.toDocument(deleteUser);
        userElasticRepository.save(userDocument);
    }

    @Override
    @Transactional
    public void block(UUID id) {
        var userEntityForBlock = getUserEntityById(id);
        if (userEntityForBlock.getBlockedAt() != null) {
            throw incorrectDataException(String.format("User with such id %s has already been blocked", id));
        }
        if (userEntityForBlock.getScope().equals(ROLE_STORAGE_MANAGER)) {
            userEntityForBlock.setBlockedAt(Instant.now());
            var blockedUser = userRepository.save(userEntityForBlock);
            var userDocument = USER_MAPPER.toDocument(blockedUser);
            userElasticRepository.save(userDocument);
        } else {
            throw incorrectDataException(String.format("User with such id %s cannot be blocked", id));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getPage(Integer page, Integer size, List<String> sort, List<String> scopes,
                              Boolean isBlocked, String search) {
        var sorting = parseSortOrDefault(sort, UserDocument.class);
        var pageable = toPageable(page, size, sorting);
        var scopesQuery = convertToUserScopeDocument(scopes);
        var userDocuments = getDocumentsPage(scopesQuery, isBlocked, search, pageable);
        if(userDocuments.isEmpty()) {
            return emptyList();
        }
        var ids = userDocuments.stream().map(UserDocument::getId).collect(Collectors.toList());
        var users = userRepository.findAllById(ids);
        return users.stream().map(USER_MAPPER::toShortModel).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public User getById(UUID id) {
        return userRepository.findById(id).map(USER_MAPPER::toShortModel).orElseThrow(() ->
                notFoundEntity(String.format("User with such %s id was not found", id)));
    }

    @Override
    @Transactional(readOnly = true)
    public UserEntity getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() ->
                notFoundEntity(String.format("User with such %s email was not found", email)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAllByUserScope(UserScope userScope) {
        var userScopeEntity = USER_MAPPER.toEntity(userScope);
        var users = userRepository.findAllByUserScope(userScopeEntity).orElseThrow(() ->
                notFoundEntity(String.format("Users with such role %s were not found", userScope)));
        return users.stream().map(USER_MAPPER::toShortModel).collect(Collectors.toList());
    }

    private UserEntity getUserEntityById(UUID id) {
        return userRepository.findById(id).orElseThrow(() ->
                notFoundEntity(String.format("User with such %s id was not found", id)));
    }

    private Page<UserDocument> getDocumentsPage(List<UserScopeDoc> scopes, Boolean isBlocked, String search, Pageable pageable) {
       if(isBlocked == null) {
           if(search == null && scopes == null) {
               return userElasticRepository.findAll(pageable);
           }
           if(search == null){
               return userElasticRepository.findAll(scopes, pageable);
           }
           if(scopes == null) {
               return userElasticRepository.findAll(search, pageable);
           }
           return userElasticRepository.findAll(scopes, search, pageable);
       } else if(isBlocked.equals(true)) {
           if(search == null && scopes == null) {
               return userElasticRepository.findAllBlocked(pageable);
           }
           if(search == null){
               return userElasticRepository.findAllBlocked(scopes, pageable);
           }
           if(scopes == null) {
               return userElasticRepository.findAllBlocked(search, pageable);
           }
           return userElasticRepository.findAllBlocked(scopes, search, pageable);
       } else {
           if(search == null && scopes == null) {
               return userElasticRepository.findAllNotBlocked(pageable);
           }
           if(search == null){
               return userElasticRepository.findAllNotBlocked(scopes, pageable);
           }
           if(scopes == null) {
               return userElasticRepository.findAllNotBlocked(search, pageable);
           }
           return userElasticRepository.findAllNotBlocked(scopes, search, pageable);
       }
    }

}
