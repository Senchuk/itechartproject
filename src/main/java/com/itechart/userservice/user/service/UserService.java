package com.itechart.userservice.user.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.ChangePasswordRequest;
import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.generated.model.UserScope;
import com.itechart.userservice.user.entity.UserEntity;

public interface UserService {

    User getCurrent();

    User update(User user);

    void changePassword(ChangePasswordRequest changePasswordRequest);

    List<User> getPage(Integer page, Integer size, List<String> sort,
                       List<String> scopes, Boolean isBlocked, String search);

    User getById(UUID id);

    void delete(UUID id);

    void block(UUID id);

    UserEntity getUserByEmail(String email);

    List<User> getAllByUserScope(UserScope userScope);

}
