package com.itechart.userservice.user.api;

import static com.itechart.userservice.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.api.UsersApi;
import com.itechart.userservice.generated.model.ChangePasswordRequest;
import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.generated.model.UserScope;
import com.itechart.userservice.user.service.UserService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class UsersApiImpl implements UsersApi {

    private final UserService userService;

    public ResponseEntity<User> getCurrentUser() {
        var user = userService.getCurrent();
        return new ResponseEntity<>(user, OK);
    }

    public ResponseEntity<User> getUserById(UUID id) {
        var user = userService.getById(id);
        return new ResponseEntity<>(user, OK);
    }

    @PreAuthorize("hasRole('ROLE_SPONSOR') or hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<List<User>> getAllUsers(Integer page, Integer size, List<String> sort, List<String> scopes,
                                                  Boolean isBlocked, String search) {
        var users = userService.getPage(page, size, sort, scopes, isBlocked, search);
        var headers = generatePaginationHeaders(users);
        return new ResponseEntity<>(users, headers, OK);
    }

    public ResponseEntity<User> updateUser(User user) {
        var userResponse = userService.update(user);
        return new ResponseEntity<>(userResponse, OK);
    }

    public ResponseEntity<Void> changePassword(ChangePasswordRequest changePasswordRequest) {
        userService.changePassword(changePasswordRequest);
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Void> blockUser(UUID id) {
        userService.block(id);
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasRole('ROLE_SPONSOR')")
    public ResponseEntity<Void> deleteUser(UUID id) {
        userService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @PreAuthorize("hasRole('ROLE_SPONSOR')")
    public ResponseEntity<List<User>> getAllUsersByUserScope(UserScope userScope) {
        var users = userService.getAllByUserScope(userScope);
        return new ResponseEntity<>(users, OK);
    }

}
