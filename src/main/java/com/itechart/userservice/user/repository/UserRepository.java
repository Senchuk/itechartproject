package com.itechart.userservice.user.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.user.entity.UserEntity;
import com.itechart.userservice.user.entity.enums.UserScopeEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    @Query("SELECT user " +
           "FROM UserEntity AS user " +
           "WHERE user.email = :email " +
           " AND user.deletedAt IS NULL")
    Optional<UserEntity> findByEmail(@Param("email") String email);

    @Query("SELECT users " +
           "FROM UserEntity AS users " +
           "WHERE users.scope = :scope " +
           " AND users.deletedAt IS NULL")
    Optional<List<UserEntity>> findAllByUserScope(@Param("scope") UserScopeEntity userScopeEntity);

    @Query("SELECT user " +
           "FROM UserEntity AS user " +
           "WHERE user.id = :id " +
           " AND user.deletedAt IS NULL")
    Optional<UserEntity> findById(@Param("id") UUID id);

    @Query("SELECT user " +
           "FROM UserEntity AS user " +
           "WHERE user.id IN (:ids)")
    List<UserEntity> findAllById(@Param("ids") List<UUID> ids);

}
