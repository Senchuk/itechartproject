package com.itechart.userservice.user.repository.elastic;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.user.document.UserDocument;
import com.itechart.userservice.user.document.enums.UserScopeDoc;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserElasticRepository extends ElasticsearchRepository<UserDocument, UUID> {

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"match\" : { \"scope\" : \"?0\"}}," +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"firstName\" : \"?1\"}}," +
           "       { \"match\" : { \"lastName\" : \"?1\"}}," +
           "       { \"match_phrase\" : { \"email\" : \"?1\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<UserDocument> findAllBlocked(List<UserScopeDoc> filter,
                                      String search,
                                      Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"match\" : { \"scope\" : \"?0\"}}," +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}" +
           "  ]" +
           " }" +
           "}")
    Page<UserDocument> findAllBlocked(List<UserScopeDoc> filter,
                                      Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"firstName\" : \"?0\"}}," +
           "       { \"match\" : { \"lastName\" : \"?0\"}}," +
           "       { \"match_phrase\" : { \"email\" : \"?0\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<UserDocument> findAllBlocked(String search,
                                      Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : " +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}" +
           " }" +
           "}")
    Page<UserDocument> findAllBlocked(Pageable pageable);


    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : [" +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}" +
           "   ]," +
           "   \"must\" : [" +
           "    { \"match\" : { \"scope\" : \"?0\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"firstName\" : \"?1\"}}," +
           "       { \"match\" : { \"lastName\" : \"?1\"}}," +
           "       { \"match_phrase\" : { \"email\" : \"?1\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<UserDocument> findAllNotBlocked(List<UserScopeDoc> filter,
                                      String search,
                                      Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : [" +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}" +
           "   ]," +
           "   \"should\" : [" +
           "    { \"match\" : { \"firstName\" : \"?0\"}}," +
           "    { \"match\" : { \"lastName\" : \"?0\"}}," +
           "    { \"match_phrase\" : { \"email\" : \"?0\"}}" +
           "  ]" +
           " }" +
           "}")
    Page<UserDocument> findAllNotBlocked(String search,
                                         Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : [" +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}" +
           "   ]," +
           "   \"must\" : " +
           "    { \"match\" : { \"scope\" : \"?0\"}}" +
           " }" +
           "}")
    Page<UserDocument> findAllNotBlocked(List<UserScopeDoc> filter,
                                         Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : [" +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "    { \"exists\" : { \"field\" : \"blockedAt\"}}" +
           "   ]" +
           " }" +
           "}")
    Page<UserDocument> findAllNotBlocked(Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : [" +
           "    { \"match\" : { \"scope\" : \"?0\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"firstName\" : \"?1\"}}," +
           "       { \"match\" : { \"lastName\" : \"?1\"}}," +
           "       { \"match_phrase\" : { \"email\" : \"?1\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<UserDocument> findAll(List<UserScopeDoc> filter,
                               String search,
                               Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"should\" : [" +
           "    { \"match\" : { \"firstName\" : \"?0\"}}," +
           "    { \"match\" : { \"lastName\" : \"?0\"}}," +
           "    { \"match_phrase\" : { \"email\" : \"?0\"}}" +
           "   ]" +
           " }" +
           "}")
    Page<UserDocument> findAll(String search,
                               Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}," +
           "   \"must\" : " +
           "    { \"match\" : { \"scope\" : \"?0\"}}" +
           " }" +
           "}")
    Page<UserDocument> findAll(List<UserScopeDoc> filter,
                               Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must_not\" : " +
           "    { \"exists\" : { \"field\" : \"deletedAt\"}}" +
           " }" +
           "}")
    Page<UserDocument> findAll(Pageable pageable);

    void deleteByScope(UserScopeDoc userScopeDoc);

}
