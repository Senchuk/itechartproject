package com.itechart.userservice.user.mapper;

import static com.itechart.userservice.generated.model.UserScope.*;

import com.itechart.userservice.generated.model.SignUpRequest;
import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.generated.model.UserScope;
import com.itechart.userservice.user.document.UserDocument;
import com.itechart.userservice.user.entity.UserEntity;
import com.itechart.userservice.user.entity.enums.UserScopeEntity;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "password", ignore = true)
    UserEntity toEntity(@MappingTarget UserEntity userEntity, User fullUser);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "blockedAt", ignore = true)
    @Mapping(target = "storageId", ignore = true)
    UserEntity toEntity(SignUpRequest signUpRequest);

    @Mapping(source = "scope", target = "scope", qualifiedByName = "scopesMapper")
    User toShortModel(UserEntity userEntity);

    @ValueMapping(target = "ROLE_SPONSOR", source = "SPONSOR")
    @ValueMapping(target = "ROLE_RESEARCHER", source = "RESEARCHER")
    @ValueMapping(target = "ROLE_STORAGE_MANAGER", source = "STORAGE_MANAGER")
    @ValueMapping(target = "ROLE_MAIN_STORAGE_MANAGER", source = "MAIN_STORAGE_MANAGER")
    UserScopeEntity toEntity(UserScope userScope);

    UserDocument toDocument(UserEntity userEntity);

    @Named("scopesMapper")
    default UserScope scopeMapper(UserScopeEntity scopeEntity) {
        switch (scopeEntity) {
            case ROLE_SPONSOR:
                return SPONSOR;
            case ROLE_RESEARCHER:
                return RESEARCHER;
            case ROLE_STORAGE_MANAGER:
                return STORAGE_MANAGER;
            case ROLE_MAIN_STORAGE_MANAGER:
                return MAIN_STORAGE_MANAGER;
        }
        return null;
    }

}
