package com.itechart.userservice.user.document;

import static javax.persistence.EnumType.STRING;

import static org.springframework.data.elasticsearch.annotations.FieldType.Text;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.itechart.userservice.user.document.enums.UserScopeDoc;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import lombok.Data;

@Data
@Document(indexName = "user", type = "_doc")
public class UserDocument {

    @Id
    private UUID id;

    @Field(type = Text, fielddata = true)
    private String firstName;

    @Field(type = Text, fielddata = true)
    private String lastName;

    @Field(type = Text, fielddata = true)
    private String email;

    @Enumerated(STRING)
    private UserScopeDoc scope;

    private Date blockedAt;

    private UUID storageId;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

}
