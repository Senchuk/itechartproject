package com.itechart.userservice.user.document.enums;

public enum UserScopeDoc {

    ROLE_SPONSOR,
    ROLE_RESEARCHER,
    ROLE_MAIN_STORAGE_MANAGER,
    ROLE_STORAGE_MANAGER

}
