package com.itechart.userservice.common;

import static com.itechart.userservice.user.document.enums.UserScopeDoc.ROLE_MAIN_STORAGE_MANAGER;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import com.itechart.userservice.user.document.UserDocument;
import com.itechart.userservice.user.repository.elastic.UserElasticRepository;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ElasticsearchDataInsert {

    private final UserElasticRepository userElasticRepository;

    @EventListener(ContextRefreshedEvent.class)
    public void init() {
        var user = new UserDocument();
        user.setId(UUID.fromString("feefda0d-01c7-430f-a75a-9dfe5eb2d1e5"));
        user.setFirstName("manager");
        user.setLastName("manager");
        user.setEmail("main@mail.com");
        user.setScope(ROLE_MAIN_STORAGE_MANAGER);
        user.setStorageId(UUID.fromString("14142e8c-b76e-4d0d-8370-8769de25ceb3"));
        user.setCreatedAt(Date.from(Instant.now()));
        userElasticRepository.save(user);
    }

}
