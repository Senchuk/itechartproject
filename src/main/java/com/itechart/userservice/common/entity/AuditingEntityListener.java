package com.itechart.userservice.common.entity;

import java.time.Instant;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class AuditingEntityListener {

    @PrePersist
    public void prePersist(AbstractEntity abstractEntity) {
        abstractEntity.setCreatedAt(Instant.now());
    }

    @PreUpdate
    public void preUpdate(AbstractEntity abstractEntity) {
        abstractEntity.setUpdatedAt(Instant.now());
    }

}
