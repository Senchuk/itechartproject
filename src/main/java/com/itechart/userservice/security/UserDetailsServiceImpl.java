package com.itechart.userservice.security;

import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;

import com.itechart.userservice.user.repository.UserRepository;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
        var userEntity = userRepository.findByEmail(username).orElseThrow(() ->
                notFoundEntity(String.format("User with such email %s was not found", username)));
        var userDetails = new UserDetailsImpl();
        userDetails.setId(userEntity.getId());
        userDetails.setEmail(userEntity.getEmail());
        userDetails.setPassword(userEntity.getPassword());
        userDetails.setScope(userEntity.getScope());
        return userDetails;
    }

}
