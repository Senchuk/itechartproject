package com.itechart.userservice.config;

import java.net.InetAddress;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.SneakyThrows;

@Configuration
public class ElasticsearchConfiguration {

    @Bean
    @SneakyThrows
    public Client elasticClient(final @Value("${elasticsearch.host}") String elasticsearchHost,
                                final @Value("${elasticsearch.port}") int elasticsearchPort,
                                final @Value("${elasticsearch.cluster-name}") String clusterName) {

        var settings = Settings.builder().put("cluster.name", clusterName).build();
        return new PreBuiltTransportClient(settings)
                .addTransportAddress(new TransportAddress(
                        InetAddress.getByName(elasticsearchHost), elasticsearchPort));
    }

}
