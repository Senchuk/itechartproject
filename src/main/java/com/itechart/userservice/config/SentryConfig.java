package com.itechart.userservice.config;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itechart.userservice.exception.*;
import com.itechart.userservice.exception.NoSuchFieldException;

import io.sentry.Sentry;
import io.sentry.spring.SentryExceptionResolver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class SentryConfig {

    @Value("${sentry.dns}")
    private String sentryDns;

    private static final List<Class<? extends Exception>> HANDLE_EXCEPTIONS = List.of(
            EntityNotFoundException.class,
            IncorrectDataException.class,
            IncorrectRequestException.class,
            InvalidAuthException.class,
            NoSuchFieldException.class,
            UserAlreadyRegisteredException.class,
            UserWasBlockedException.class
    );

    @Bean
    void init() {
        Sentry.init(sentryDns);
    }

    @Bean
    public HandlerExceptionResolver sentryExceptionResolver() {
        return new SentryExceptionResolver() {
            @Override
            public ModelAndView resolveException(HttpServletRequest request,
                                                 HttpServletResponse response,
                                                 Object handler,
                                                 Exception ex){
                if (HANDLE_EXCEPTIONS.contains(ex.getClass())) {
                    return null;
                }
                 return super.resolveException(request, response, handler, ex);
            }
        };
    }

}
