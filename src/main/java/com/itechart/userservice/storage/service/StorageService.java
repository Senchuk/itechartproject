package com.itechart.userservice.storage.service;

import java.util.UUID;

import com.itechart.userservice.generated.model.Storage;

public interface StorageService {

    Storage create(UUID hospitalId);

    Storage getByHospitalId(UUID hospitalId);

    Storage getById(UUID storageId);

}
