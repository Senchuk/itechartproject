package com.itechart.userservice.storage.service.impl;

import static com.itechart.userservice.storage.mapper.StorageMapper.STORAGE_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;

import java.util.UUID;

import com.itechart.userservice.generated.model.Storage;
import com.itechart.userservice.storage.entity.StorageEntity;
import com.itechart.userservice.storage.repository.StorageRepository;
import com.itechart.userservice.storage.service.StorageService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StorageServiceImpl implements StorageService {

    private final StorageRepository storageRepository;

    @Override
    @Transactional
    public Storage create(UUID hospitalId) {
        var storage = new StorageEntity();
        storage.setHospitalId(hospitalId);
        var savedStorage = storageRepository.save(storage);
        return STORAGE_MAPPER.toModel(savedStorage);
    }

    @Override
    @Transactional(readOnly = true)
    public Storage getByHospitalId(UUID hospitalId) {
        var storage = storageRepository.findByHospitalId(hospitalId).orElseThrow(() -> {
            throw notFoundEntity(String.format("Storage with such hospital id %s was not found", hospitalId));
        });
        return STORAGE_MAPPER.toModel(storage);
    }

    @Override
    @Transactional(readOnly = true)
    public Storage getById(UUID id) {
        var storageEntity = storageRepository.findById(id).orElseThrow(()->
            notFoundEntity(String.format("Storage with such %s id was not found", id)));
        return STORAGE_MAPPER.toModel(storageEntity);
    }

}
