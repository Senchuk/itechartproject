package com.itechart.userservice.storage.mapper;

import com.itechart.userservice.generated.model.Storage;
import com.itechart.userservice.storage.entity.StorageEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StorageMapper {

    StorageMapper STORAGE_MAPPER = Mappers.getMapper(StorageMapper.class);

    StorageEntity toEntity(Storage storage);

    Storage toModel(StorageEntity storageEntity);

}
