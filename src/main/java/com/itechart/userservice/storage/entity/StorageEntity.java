package com.itechart.userservice.storage.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.UUID;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "storage")
public class StorageEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "hospital_id")
    private UUID hospitalId;

}

