package com.itechart.userservice.storage.repository;

import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.storage.entity.StorageEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageRepository extends JpaRepository<StorageEntity, UUID> {

    Optional<StorageEntity> findByHospitalId(UUID hospitalId);

    @Modifying
    @Query("DELETE FROM StorageEntity as storage " +
           "WHERE storage.hospitalId IS NOT NULL")
    void deleteAll();

}
