package com.itechart.userservice.hospital.api;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;

import com.itechart.userservice.generated.api.HospitalsApi;
import com.itechart.userservice.generated.model.Hospital;
import com.itechart.userservice.hospital.service.HospitalService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class HospitalsApiImpl implements HospitalsApi {

    private final HospitalService hospitalService;

    @PreAuthorize("hasRole('ROLE_SPONSOR')")
    public ResponseEntity<Hospital> createHospital(Hospital hospital) {
        var hospitalResponse = hospitalService.create(hospital);
        return new ResponseEntity<>(hospitalResponse, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER') or hasRole('ROLE_SPONSOR')")
    public ResponseEntity<List<Hospital>> getAllHospitals() {
        var hospitalResponse = hospitalService.getAll();
        return new ResponseEntity<>(hospitalResponse, OK);
    }

}
