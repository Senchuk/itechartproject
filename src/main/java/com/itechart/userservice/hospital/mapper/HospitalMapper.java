package com.itechart.userservice.hospital.mapper;

import com.itechart.userservice.generated.model.Hospital;
import com.itechart.userservice.hospital.entity.HospitalEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface HospitalMapper {

    HospitalMapper HOSPITAL_MAPPER = Mappers.getMapper(HospitalMapper.class);

    HospitalEntity toEntity(Hospital hospital);

    Hospital toModel(HospitalEntity hospitalEntity);

}
