package com.itechart.userservice.hospital.repository;

import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.hospital.entity.HospitalEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HospitalRepository extends JpaRepository<HospitalEntity, UUID> {

    Boolean existsByManagerId(UUID managerId);

    Optional<HospitalEntity> findByManagerId(UUID managerId);

}
