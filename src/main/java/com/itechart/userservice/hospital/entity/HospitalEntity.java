package com.itechart.userservice.hospital.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.UUID;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "hospital")
public class HospitalEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "manager_id", nullable = false)
    private UUID managerId;

}
