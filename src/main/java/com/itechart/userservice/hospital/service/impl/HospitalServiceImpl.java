package com.itechart.userservice.hospital.service.impl;

import static com.itechart.userservice.generated.model.UserScope.STORAGE_MANAGER;
import static com.itechart.userservice.hospital.mapper.HospitalMapper.HOSPITAL_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.incorrectDataException;
import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.Hospital;
import com.itechart.userservice.hospital.repository.HospitalRepository;
import com.itechart.userservice.hospital.service.HospitalService;
import com.itechart.userservice.storage.service.StorageService;
import com.itechart.userservice.user.service.UserService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class HospitalServiceImpl implements HospitalService {

    private final StorageService storageService;
    private final UserService userService;

    private final HospitalRepository hospitalRepository;

    @Override
    @Transactional
    public Hospital create(Hospital hospital) {
        var user = userService.getById(hospital.getManagerId());
        if (!user.getScope().equals(STORAGE_MANAGER)) {
            throw incorrectDataException(
                    String.format("User with such id %s cannot be manager of the hospital", user.getId()));
        }
        var isManagerBusy = hospitalRepository.existsByManagerId(user.getId());
        if (isManagerBusy) {
            throw incorrectDataException(
                    String.format("Manager with such id %s already refers to another hospital", user.getId()));
        }
        var hospitalEntity = HOSPITAL_MAPPER.toEntity(hospital);
        var savedHospital = hospitalRepository.save(hospitalEntity);
        var savedStorage = storageService.create(savedHospital.getId());
        user.setStorageId(savedStorage.getId());
        userService.update(user);
        return HOSPITAL_MAPPER.toModel(savedHospital);
    }

    @Override
    @Transactional(readOnly = true)
    public Hospital getById(UUID id) {
        return hospitalRepository.findById(id).map(HOSPITAL_MAPPER::toModel).orElseThrow(() ->
                notFoundEntity(String.format("Hospital with such %s id was not found", id)));
    }

    @Override
    @Transactional(readOnly = true)
    public Hospital getByManagerId(UUID managerId) {
        var hospital = hospitalRepository.findByManagerId(managerId).orElseThrow(() -> {
            throw notFoundEntity(String.format("Hospital with such manager id %s was not found", managerId));
        });
        return HOSPITAL_MAPPER.toModel(hospital);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Hospital> getAll() {
        var hospitals = hospitalRepository.findAll();
        return hospitals.stream().map(HOSPITAL_MAPPER::toModel).collect(Collectors.toList());
    }

}
