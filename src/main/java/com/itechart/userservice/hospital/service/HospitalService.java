package com.itechart.userservice.hospital.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.Hospital;

public interface HospitalService {

    Hospital create(Hospital hospital);

    List<Hospital> getAll();

    Hospital getById(UUID id);

    Hospital getByManagerId(UUID id);

}
