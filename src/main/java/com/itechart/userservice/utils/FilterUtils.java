package com.itechart.userservice.utils;

import static org.springframework.util.CollectionUtils.isEmpty;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.medicine.document.enums.MedicineTypeDocument;
import com.itechart.userservice.patient.document.enums.PatientStatusDocument;
import com.itechart.userservice.user.document.enums.UserScopeDoc;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FilterUtils {

    public static List<UserScopeDoc> convertToUserScopeDocument(List<String> filter) {
        if (isEmpty(filter)) {
            return null;
        }
        return filter.stream().map(UserScopeDoc::valueOf).collect(Collectors.toList());
    }

    public static List<PatientStatusDocument> convertToPatientStatusDocument(List<String> filter) {
        if (isEmpty(filter)) {
            return null;
        }
        return filter.stream().map(PatientStatusDocument::valueOf).collect(Collectors.toList());
    }

    public static List<MedicineTypeDocument> convertToMedicineTypeDocument(List<String> filter) {
        if (isEmpty(filter)) {
            return null;
        }
        return filter.stream().map(MedicineTypeDocument::valueOf).collect(Collectors.toList());
    }

    public static List<UUID> filterByStorageId(Boolean isLocalStorage, UUID userStorageId, UUID mainStorageId) {
        var filter = new ArrayList<UUID>();
        if (isLocalStorage == null) {
            filter.add(userStorageId);
            filter.add(mainStorageId);
        } else if(isLocalStorage) {
            filter.add(userStorageId);
        } else {
            filter.add(mainStorageId);
        }
        return filter;
    }

}
