package com.itechart.userservice.utils;

import java.util.List;

import lombok.experimental.UtilityClass;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

@UtilityClass
public class HeaderUtils {

    private static final String X_TOTAL_COUNT = "X-Total-Count";

    public static <T> MultiValueMap<String, String> generatePaginationHeaders(Page<T> page) {
        var headers = new HttpHeaders();
        headers.add(X_TOTAL_COUNT, Long.toString(page.getTotalElements()));
        return headers;
    }

    public static <T> MultiValueMap<String, String> generatePaginationHeaders(List<T> page) {
        var headers = new HttpHeaders();
        headers.add(X_TOTAL_COUNT, Long.toString(page.size()));
        return headers;
    }

}
