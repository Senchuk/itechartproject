package com.itechart.userservice.utils;

import com.itechart.userservice.exception.*;
import com.itechart.userservice.exception.NoSuchFieldException;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ExceptionUtils {

    public static EntityNotFoundException notFoundEntity(String message) {
        return new EntityNotFoundException(message);
    }

    public static UserAlreadyRegisteredException userAlreadyRegistered(String message) {
        return new UserAlreadyRegisteredException(message);
    }

    public static IncorrectDataException incorrectDataException(String message) {
        return new IncorrectDataException(message);
    }

    static NoSuchFieldException fieldNotFoundException(String message) {
        return new NoSuchFieldException(message);
    }

    public static IncorrectRequestException incorrectRequestException(String message) {
        return new IncorrectRequestException(message);
    }

    public static InvalidAuthException invalidAuthException(String message) {
        return new InvalidAuthException(message);
    }

    public static UserWasBlockedException userWasBlockedException(String message) {
        return new UserWasBlockedException(message);
    }

    public static ElasticsearchQueryException elasticsearchQueryException(String message) {
        return new ElasticsearchQueryException(message);
    }

}
