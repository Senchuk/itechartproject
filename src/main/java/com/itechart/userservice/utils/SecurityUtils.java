package com.itechart.userservice.utils;

import com.itechart.userservice.security.UserDetailsImpl;

import lombok.experimental.UtilityClass;

import org.springframework.security.core.context.SecurityContextHolder;

@UtilityClass
public class SecurityUtils {

    public static UserDetailsImpl getCurrentUser() {
        return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
