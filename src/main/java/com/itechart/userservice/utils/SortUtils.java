package com.itechart.userservice.utils;

import static com.itechart.userservice.utils.ExceptionUtils.fieldNotFoundException;
import static com.itechart.userservice.utils.ExceptionUtils.incorrectDataException;

import static org.springframework.data.domain.Sort.Direction.DESC;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.Inventory;
import com.itechart.userservice.generated.model.Medicine;

import lombok.experimental.UtilityClass;

import org.springframework.data.domain.Sort;

@UtilityClass
public class SortUtils {

    public static Sort parseSortOrDefault(List<String> sortParam, Class<?> model) {
        if (sortParam == null) {
            return Sort.by(DESC, "createdAt");
        } else {
            var sortQuery = sortParam.stream()
                    .map(param -> parseSort(param, model)).collect(Collectors.toList());
            return Sort.by(sortQuery);
        }
    }

    private Sort.Order parseSort(String sortParam, Class<?> model)  {
        var directionValue = sortParam.substring(0, 1);
        var columnValue = sortParam.substring(1);
        if (directionValue.equals("+") || directionValue.equals("-")) {
            if(model.equals(Inventory.class)) {
                validateSortInventory(columnValue, model);
            } else {
                validateSort(columnValue, model);
            }
            return directionValue.equals("-") ?
                    Sort.Order.desc(columnValue) : Sort.Order.asc(columnValue);
        } else {
            throw incorrectDataException("Invalid sort type");
        }
    }

    private void validateSort(String sortParams, Class<?> model) {
        try {
            Optional.ofNullable(model.getDeclaredField(sortParams))
                    .orElseThrow(NoSuchFieldException::new);
        } catch (NoSuchFieldException e) {
            throw fieldNotFoundException("Such field is not found");
        }
    }

    private void validateSortInventory(String sortParam, Class<?> model) {
        try {
            Optional.ofNullable(Medicine.class.getDeclaredField(sortParam))
                    .orElseThrow(NoSuchFieldException::new);
            throw incorrectDataException(String.format("Sorting cannot be performed on this field %s", sortParam));
        } catch (NoSuchFieldException ex) {
            try {
                Optional.ofNullable(model.getDeclaredField(sortParam))
                        .orElseThrow(NoSuchFieldException::new);
            } catch (NoSuchFieldException e) {
                throw fieldNotFoundException("Such field is not found");
            }
        }
    }

}
