package com.itechart.userservice.inventory.service;

public interface InventoryElasticService {

    void deleteWithExpiredMedicines();

}
