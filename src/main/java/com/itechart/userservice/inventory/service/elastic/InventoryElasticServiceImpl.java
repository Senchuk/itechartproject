package com.itechart.userservice.inventory.service.elastic;

import static com.itechart.userservice.utils.ExceptionUtils.elasticsearchQueryException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.inventory.entity.InventoryEntity;
import com.itechart.userservice.inventory.repository.InventoryRepository;
import com.itechart.userservice.inventory.service.InventoryElasticService;
import com.itechart.userservice.medicine.entity.MedicineEntity;
import com.itechart.userservice.medicine.repository.MedicineRepository;

import org.elasticsearch.client.Client;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class InventoryElasticServiceImpl implements InventoryElasticService {

    private final static String INVENTORY_INDEX = "inventory";
    private final static String INVENTORY_TYPE = "_doc";

    private final InventoryRepository inventoryRepository;
    private final MedicineRepository medicineRepository;

    private final ApplicationContext applicationContext;

    public void deleteWithExpiredMedicines() {
        var expiredMedicines = medicineRepository.findAllExpiredToday();
        if (expiredMedicines.isEmpty()) {
            return;
        }
        var medicinesIds = expiredMedicines.stream().map(MedicineEntity::getId).collect(Collectors.toList());
        var inventoriesWithExpiredMedicines = inventoryRepository.findAllByMedicineId(medicinesIds);

        if(inventoriesWithExpiredMedicines.isEmpty()) {
            return;
        }
        var inventoryIds = inventoriesWithExpiredMedicines.stream()
                .map(InventoryEntity::getId).collect(Collectors.toList());
        softDelete(inventoryIds);
    }

    private void softDelete(List<UUID> inventoryIds) {
        var client  = applicationContext.getBean(Client.class);
        var bulkRequest = client.prepareBulk();
        for (UUID id : inventoryIds) {
            try {
                bulkRequest.add(client.prepareUpdate(INVENTORY_INDEX, INVENTORY_TYPE, id.toString())
                        .setDoc(jsonBuilder()
                                .startObject()
                                .field("deleteAt", Date.from(Instant.now()))
                                .endObject()));
            } catch (IOException e) {
                throw elasticsearchQueryException("Cannot add object to elasticsearch query");
            }
        }
        var bulkResponse = bulkRequest.get();
        if(bulkResponse.hasFailures()) {
            throw elasticsearchQueryException("Elasticsearch query error");
        }
    }

}
