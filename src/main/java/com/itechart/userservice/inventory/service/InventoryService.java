package com.itechart.userservice.inventory.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.CreateInventoryRequest;
import com.itechart.userservice.generated.model.Inventory;
import com.itechart.userservice.generated.model.Order;

public interface InventoryService {

    CreateInventoryRequest create(CreateInventoryRequest createInventoryRequest);

    List<Inventory> getPage(Integer page, Integer size, List<String> sort,
                            Boolean isLocalStorage, String search);

    Integer getAvailableMedicinesQuantity(UUID medicineId);

    void getMedicineAccordingToOrder(UUID id);

    void delete(UUID id);

    Inventory setMedicineAccordingToOrder(Order order);

    void deleteWithExpiredMedicines();

}
