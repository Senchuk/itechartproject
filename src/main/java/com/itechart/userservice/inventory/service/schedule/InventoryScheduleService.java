package com.itechart.userservice.inventory.service.schedule;

import com.itechart.userservice.inventory.service.InventoryService;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class InventoryScheduleService  {

    private final InventoryService inventoryService;

    @Scheduled(cron = "00 00 00 * * ?")
    public void deleteExpiredMedicinesFromInventories() {
        inventoryService.deleteWithExpiredMedicines();
    }

}
