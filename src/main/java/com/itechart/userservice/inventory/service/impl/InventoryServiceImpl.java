package com.itechart.userservice.inventory.service.impl;

import static java.util.Collections.emptyList;

import static com.itechart.userservice.inventory.mapper.InventoryMapper.INVENTORY_MAPPER;
import static com.itechart.userservice.medicine.mapper.MedicineMapper.MEDICINE_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.incorrectDataException;
import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;
import static com.itechart.userservice.utils.FilterUtils.filterByStorageId;
import static com.itechart.userservice.utils.PagingUtils.toPageable;
import static com.itechart.userservice.utils.SortUtils.parseSortOrDefault;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.exception.IncorrectDataException;
import com.itechart.userservice.generated.model.CreateInventoryRequest;
import com.itechart.userservice.generated.model.Inventory;
import com.itechart.userservice.generated.model.Order;
import com.itechart.userservice.inventory.document.InventoryDocument;
import com.itechart.userservice.inventory.entity.InventoryEntity;
import com.itechart.userservice.inventory.repository.InventoryRepository;
import com.itechart.userservice.inventory.repository.elastic.InventoryElasticRepository;
import com.itechart.userservice.inventory.repository.jdbc.InventoryJdbcRepository;
import com.itechart.userservice.inventory.service.InventoryElasticService;
import com.itechart.userservice.inventory.service.InventoryService;
import com.itechart.userservice.medicine.repository.jdbc.MedicineJdbcRepository;
import com.itechart.userservice.medicine.service.MedicineElasticService;
import com.itechart.userservice.medicine.service.MedicineService;
import com.itechart.userservice.order.repository.OrderRepository;
import com.itechart.userservice.storage.repository.StorageRepository;
import com.itechart.userservice.storage.service.StorageService;
import com.itechart.userservice.user.service.UserService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class InventoryServiceImpl implements InventoryService {

    private final MedicineService medicineService;
    private final StorageService storageService;
    private final UserService userService;

    private final InventoryElasticService inventoryElasticService;
    private final MedicineElasticService medicineElasticService;

    private final InventoryRepository inventoryRepository;
    private final OrderRepository orderRepository;
    private final StorageRepository storageRepository;

    private final InventoryElasticRepository inventoryElasticRepository;

    private final MedicineJdbcRepository medicineJdbcRepository;
    private final InventoryJdbcRepository inventoryJdbcRepository;

    @Override
    @Transactional
    public CreateInventoryRequest create(CreateInventoryRequest createInventoryRequest) {
        var storage = storageRepository.findByHospitalId(null).orElseThrow(() -> {
            throw notFoundEntity("Default storage was not found");
        });
        var medicineId = createInventoryRequest.getMedicineId();
        var medicine = medicineService.getById(medicineId);
        var medicineEntity = MEDICINE_MAPPER.toEntity(medicine);
        var inventoryEntity = new InventoryEntity();
        inventoryEntity.setStorageId(storage.getId());
        inventoryEntity.setMedicine(medicineEntity);
        inventoryEntity.setQuantity(createInventoryRequest.getQuantity());
        var savedInventory = inventoryRepository.save(inventoryEntity);
        var inventoryDocument = INVENTORY_MAPPER.toDocument(savedInventory);
        inventoryElasticRepository.save(inventoryDocument);
        return INVENTORY_MAPPER.toShortModel(savedInventory);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inventory> getPage(Integer page, Integer size, List<String> sort,
                                   Boolean isLocalStorage, String search) {
        var user = userService.getCurrent();
        var userStorageId = user.getStorageId();
        var storage = storageRepository.findByHospitalId(null).orElseThrow(() -> {
            throw notFoundEntity("Default storage was not found");
        });
        var filter = filterByStorageId(isLocalStorage, userStorageId, storage.getId());
        var sorting = parseSortOrDefault(sort, InventoryDocument.class);
        var pageable = toPageable(page, size, sorting);
        var inventoryDocuments = getDocumentsPage(filter, search, pageable);
        if (inventoryDocuments.isEmpty()) {
            return emptyList();
        }
        var ids = inventoryDocuments.stream().map(InventoryDocument::getId).collect(Collectors.toList());
        var inventories = inventoryRepository.findAllById(ids);
        return inventories.stream().map(INVENTORY_MAPPER::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getAvailableMedicinesQuantity(UUID medicineId) {
        var defaultStorageId = storageService.getByHospitalId(null).getId();
        return inventoryRepository.getAvailableMedicinesQuantity(medicineId, defaultStorageId).orElse(0);
    }

    @Override
    @Transactional
    public void getMedicineAccordingToOrder(UUID orderId) {
        var orderEntity = orderRepository.findById(orderId).orElseThrow(() -> {
           throw notFoundEntity(String.format("There is no such order id %s in the inventory", orderId));
        });
        var countAvailableQuantity = getAvailableMedicinesQuantity(orderEntity.getMedicineId());
        if (countAvailableQuantity < orderEntity.getQuantity()) {
            throw new IncorrectDataException("Required quantity of medicine was not found");
        }
        var storage = storageRepository.findByHospitalId(null).orElseThrow(() -> {
            throw notFoundEntity("Default storage was not found");
        });
        var inventory = inventoryRepository.findByStorageIdAndMedicineId(storage.getId(), orderEntity.getMedicineId())
                .orElseThrow(() -> {
            throw notFoundEntity(
                    String.format("Inventory with such storage id %s and medicine id %s was not found",
                            storage.getId(), orderEntity.getMedicineId()));
        });
        var medicineQuantity = countAvailableQuantity - orderEntity.getQuantity();
        if (medicineQuantity == 0) {
            inventoryRepository.deleteById(inventory.getId());
        } else {
            inventory.setQuantity(medicineQuantity);
            var savedInventory = inventoryRepository.save(inventory);
            var inventoryDocument = INVENTORY_MAPPER.toDocument(savedInventory);
            inventoryElasticRepository.save(inventoryDocument);
        }
    }

    @Override
    @Transactional
    public Inventory setMedicineAccordingToOrder(Order order) {
        var medicine = medicineService.getById(order.getMedicineId());
        var inventory = new InventoryEntity();
        inventory.setMedicine(MEDICINE_MAPPER.toEntity(medicine));
        inventory.setQuantity(order.getQuantity());
        inventory.setOrderId(order.getId());
        var userStorageId = userService.getCurrent().getStorageId();
        inventory.setStorageId(userStorageId);
        var savedInventory = inventoryRepository.save(inventory);
        var inventoryDocument = INVENTORY_MAPPER.toDocument(savedInventory);
        inventoryElasticRepository.save(inventoryDocument);
        return INVENTORY_MAPPER.toModel(savedInventory);
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        var user = userService.getCurrent();
        if (user.getStorageId() == null) {
            throw incorrectDataException(
                    String.format("Manager with such id %s does not apply to any of the hospitals", user.getId()));
        }
        inventoryRepository.deleteByIdAndStorageId(id, user.getStorageId());
        inventoryElasticRepository.deleteByIdAndStorageId(id, user.getStorageId());
    }

    @Override
    @Transactional
    public void deleteWithExpiredMedicines() {
        medicineJdbcRepository.deleteExpired();
        inventoryJdbcRepository.deleteWithExpiredMedicines();

        medicineElasticService.deleteExpired();
        inventoryElasticService.deleteWithExpiredMedicines();
    }

    private Page<InventoryDocument> getDocumentsPage(List<UUID> filter, String search, Pageable pageable) {
        if (search == null) {
            return inventoryElasticRepository.findAll(filter, pageable);
        }
        return inventoryElasticRepository.findAll(filter, search, pageable);
    }

}
