package com.itechart.userservice.inventory.document;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Id;

import com.itechart.userservice.medicine.document.MedicineDocument;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Data;

@Data
@Document(indexName = "inventory", type = "_doc")
public class InventoryDocument {

    @Id
    private UUID id;

    private Integer quantity;

    @Field(type = FieldType.Object)
    private MedicineDocument medicine;

    private UUID storageId;

    private UUID orderId;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

}
