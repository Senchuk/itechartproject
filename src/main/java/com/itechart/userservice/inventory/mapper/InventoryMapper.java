package com.itechart.userservice.inventory.mapper;

import com.itechart.userservice.generated.model.CreateInventoryRequest;
import com.itechart.userservice.generated.model.Inventory;
import com.itechart.userservice.inventory.document.InventoryDocument;
import com.itechart.userservice.inventory.entity.InventoryEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface InventoryMapper {

    InventoryMapper INVENTORY_MAPPER = Mappers.getMapper(InventoryMapper.class);

    InventoryEntity toEntity(Inventory inventory);

    @Mapping(source = "medicine.id", target = "medicineId")
    CreateInventoryRequest toShortModel(InventoryEntity inventoryEntity);

    Inventory toModel(InventoryEntity inventoryEntity);

    InventoryDocument toDocument(InventoryEntity inventoryEntity);

}
