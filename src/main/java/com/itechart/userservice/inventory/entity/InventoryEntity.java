package com.itechart.userservice.inventory.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.UUID;
import javax.persistence.*;

import com.itechart.userservice.common.entity.AbstractEntity;
import com.itechart.userservice.medicine.entity.MedicineEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "inventory")
@EqualsAndHashCode(callSuper = true)
public class InventoryEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name="medicine_id", nullable=false)
    private MedicineEntity medicine;

    @Column(name = "storage_id", nullable = false)
    private UUID storageId;

    @Column(name = "order_id")
    private UUID orderId;

}
