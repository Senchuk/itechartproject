package com.itechart.userservice.inventory.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.inventory.entity.InventoryEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends JpaRepository<InventoryEntity, UUID> {

    @Query("SELECT SUM(inventory.quantity) " +
           "FROM InventoryEntity AS inventory " +
           "WHERE inventory.medicine.id = :medicineId " +
           " AND inventory.storageId = :storageId")
    Optional<Integer> getAvailableMedicinesQuantity(@Param("medicineId") UUID medicineId,
                                                    @Param("storageId") UUID storageId);

    @Query("SELECT inventory " +
           "FROM InventoryEntity AS inventory " +
           "WHERE (inventory.storageId IN :storageId)" +
           " AND ((:search) IS NULL OR " +
           "      inventory.medicine.name = :search OR " +
           "      inventory.medicine.producer = :search OR " +
           "      inventory.medicine.description = :search)")
    Page<InventoryEntity> findAll(@Param("storageId") List<UUID> storageId,
                                  @Param("search") String search,
                                  Pageable pageable);

    Optional<InventoryEntity> findByStorageIdAndMedicineId(UUID storageId, UUID medicineId);

    void deleteByIdAndStorageId(UUID id, UUID storageId);

    @Query("SELECT inventory " +
           "FROM InventoryEntity AS inventory " +
           "WHERE inventory.id IN (:ids)")
    List<InventoryEntity> findAllById(@Param("ids") List<UUID> ids);

    @Query("SELECT inventory " +
           "FROM InventoryEntity AS inventory " +
           "WHERE inventory.medicine.id IN (:medicineIds)")
    List<InventoryEntity> findAllByMedicineId(@Param("medicineIds") List<UUID> medicineIds);

}
