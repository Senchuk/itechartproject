package com.itechart.userservice.inventory.repository.elastic;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.inventory.document.InventoryDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryElasticRepository extends ElasticsearchRepository<InventoryDocument, UUID> {

    @Query("{" +
           " \"bool\" : {" +
           "   \"must\" : [" +
           "    { \"match\" : { \"storageId\" : \"?0\"}}," +
           "    {" +
           "     \"bool\" : {" +
           "      \"should\" : [" +
           "       { \"match\" : { \"medicine.name\" : \"?1\"}}," +
           "       { \"match\" : { \"medicine.producer\" : \"?1\"}}," +
           "       { \"match_phrase\" : { \"medicine.description\" : \"?1\"}}" +
           "      ]" +
           "     }" +
           "    }" +
           "  ]" +
           " }" +
           "}")
    Page<InventoryDocument> findAll(List<UUID> storageId,
                                    String search,
                                    Pageable pageable);

    @Query("{" +
           " \"bool\" : {" +
           "   \"must\" : " +
           "    { \"match\" : { \"storageId\" : \"?0\"}}" +
           " }" +
           "}")
    Page<InventoryDocument> findAll(List<UUID> storageId,
                                   Pageable pageable);

    void deleteByIdAndStorageId(UUID id, UUID storageId);

}
