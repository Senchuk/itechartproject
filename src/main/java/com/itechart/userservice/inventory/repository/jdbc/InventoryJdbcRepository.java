package com.itechart.userservice.inventory.repository.jdbc;

import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;

import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class InventoryJdbcRepository {

    private static final String SELECT_RANDOM_MEDICINE =
            "SELECT medicine_id FROM Inventory WHERE storage_id = :storageId ORDER BY random() limit 1";

    private static final String SOFT_DELETE_INVENTORY = "UPDATE inventory SET deleted_at = CURRENT_TIMESTAMP " +
            "FROM medicine WHERE inventory.medicine_id = medicine.id AND " +
            "inventory.deleted_at IS NULL AND medicine.deleted_at IS NOT NULL";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    public UUID getRandomMedicineByStorageId(UUID storageId) {
        var params = new HashMap<String, Object>();
        params.put("storageId", storageId);
        return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SELECT_RANDOM_MEDICINE, params, UUID.class))
                .orElseThrow(() -> {
                    throw notFoundEntity(String.format("There was no inventory with such storage id %s", storageId));
                });
    }

    public void deleteWithExpiredMedicines() {
        jdbcTemplate.update(SOFT_DELETE_INVENTORY);
    }

}
