package com.itechart.userservice.inventory.api;

import static com.itechart.userservice.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.*;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.api.InventoriesApi;
import com.itechart.userservice.generated.model.CreateInventoryRequest;
import com.itechart.userservice.generated.model.Inventory;
import com.itechart.userservice.inventory.service.InventoryService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class InventoriesApiImpl implements InventoriesApi {

    private final InventoryService inventoryService;

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER')")
    public ResponseEntity<CreateInventoryRequest> createInventory(CreateInventoryRequest createInventoryRequest) {
        var inventoryResponse = inventoryService.create(createInventoryRequest);
        return new ResponseEntity<>(inventoryResponse, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER')")
    public ResponseEntity<Integer> getAvailableMedicinesQuantity(UUID medicineId) {
        var response = inventoryService.getAvailableMedicinesQuantity(medicineId);
        return new ResponseEntity<>(response, OK);
    }

    @PreAuthorize("hasRole('ROLE_STORAGE_MANAGER') or hasRole('ROLE_MAIN_STORAGE_MANAGER')")
    public ResponseEntity<List<Inventory>> getAllInventory(Integer page, Integer size, List<String> sort,
                                                           Boolean isLocalStorage, String search) {
        var inventories = inventoryService.getPage(page, size, sort, isLocalStorage, search);
        var headers = generatePaginationHeaders(inventories);
        return new ResponseEntity<>(inventories, headers, OK);
    }

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER') or hasRole('ROLE_STORAGE_MANAGER')")
    public ResponseEntity<Void> deleteInventory(UUID id) {
        inventoryService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
