package com.itechart.userservice.visit.entity;

import java.time.Instant;
import java.util.Date;
import javax.persistence.PrePersist;

public class VisitAuditingEntityListener {

    @PrePersist
    public void prePersist(VisitEntity visitEntity) {
        visitEntity.setDate(Date.from(Instant.now()));
    }

}
