package com.itechart.userservice.visit.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.Date;
import java.util.UUID;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "visit")
@EntityListeners(VisitAuditingEntityListener.class)
public class VisitEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "patient_id", nullable = false)
    private UUID patientId;

    @Column(name = "medicine_id", nullable = false)
    private UUID medicineId;

}
