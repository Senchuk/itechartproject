package com.itechart.userservice.visit.mapper;

import com.itechart.userservice.generated.model.Visit;
import com.itechart.userservice.visit.entity.VisitEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VisitMapper {

    VisitMapper VISIT_MAPPER = Mappers.getMapper(VisitMapper.class);

    VisitEntity toEntity(Visit visit);

    Visit toModel(VisitEntity visitEntity);

}
