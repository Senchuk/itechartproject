package com.itechart.userservice.visit.api;

import static org.springframework.http.HttpStatus.CREATED;

import java.util.UUID;

import com.itechart.userservice.generated.api.VisitsApi;
import com.itechart.userservice.generated.model.Visit;
import com.itechart.userservice.visit.service.VisitService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class VisitsApiImpl implements VisitsApi {

    private final VisitService visitService;

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Visit> createVisitByPatientId(UUID patientId) {
        var visitResponse = visitService.createByPatientId(patientId);
        return new ResponseEntity<>(visitResponse, CREATED);
    }

}
