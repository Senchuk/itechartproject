package com.itechart.userservice.visit.service;

import java.util.UUID;

import com.itechart.userservice.generated.model.Visit;

public interface VisitService {

    Visit createByPatientId(UUID id);

}
