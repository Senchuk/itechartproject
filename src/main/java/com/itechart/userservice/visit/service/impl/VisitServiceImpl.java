package com.itechart.userservice.visit.service.impl;

import static com.itechart.userservice.utils.ExceptionUtils.incorrectDataException;
import static com.itechart.userservice.visit.mapper.VisitMapper.VISIT_MAPPER;

import java.util.UUID;

import com.itechart.userservice.generated.model.Visit;
import com.itechart.userservice.medicine.service.MedicineService;
import com.itechart.userservice.patient.service.PatientService;
import com.itechart.userservice.user.service.UserService;
import com.itechart.userservice.visit.entity.VisitEntity;
import com.itechart.userservice.visit.repository.VisitRepository;
import com.itechart.userservice.visit.service.VisitService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class VisitServiceImpl implements VisitService {

    private final MedicineService medicineService;
    private final PatientService patientService;
    private final UserService userService;

    private final VisitRepository visitRepository;

    @Override
    @Transactional
    public Visit createByPatientId(UUID patientId) {
        var researcher = userService.getCurrent();
        if(researcher.getBlockedAt() != null) {
            throw incorrectDataException("You account was blocked. You cannot create patient visit");
        }
        var patient = patientService.getById(patientId);
        if(!patient.getResearcherId().equals(researcher.getId())) {
            throw incorrectDataException(String.format("Patient with such id %s is not your patient", patient.getId()));
        }
        var visit = new VisitEntity();
        visit.setPatientId(patientId);
        var isVisitedBefore = visitRepository.existsByPatientId(patientId);
        if (isVisitedBefore) {
            var medicine = medicineService.getRandom();
            visit.setMedicineId(medicine.getId());
        }
        var savedVisit = visitRepository.save(visit);
        return VISIT_MAPPER.toModel(savedVisit);
    }

}
