package com.itechart.userservice.visit.repository;

import java.util.UUID;

import com.itechart.userservice.visit.entity.VisitEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitRepository extends JpaRepository<VisitEntity, UUID> {

    Boolean existsByPatientId(UUID patientId);

    @Query("SELECT COUNT(visit) " +
           "FROM VisitEntity AS visit")
    Integer countTotalVisitQuantity();

    @Query("SELECT COUNT(visit) " +
           "FROM VisitEntity AS visit " +
           "WHERE visit.medicineId IS NOT NULL")
    Integer countTotalDistributedMedicineQuantity();

}
