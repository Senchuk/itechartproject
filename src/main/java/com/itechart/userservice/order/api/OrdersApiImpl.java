package com.itechart.userservice.order.api;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.api.OrdersApi;
import com.itechart.userservice.generated.model.*;
import com.itechart.userservice.order.service.OrderRequestService;
import com.itechart.userservice.order.service.OrderService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class OrdersApiImpl implements OrdersApi {

    private final OrderService orderService;
    private final OrderRequestService orderRequestService;

    @PreAuthorize("hasRole('ROLE_STORAGE_MANAGER')")
    public ResponseEntity<Order> createOrder(CreateOrder createOrder) {
        var orderResponse = orderService.create(createOrder);
        return new ResponseEntity<>(orderResponse, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_STORAGE_MANAGER')")
    public ResponseEntity<List<Order>> getAllOrders() {
        var ordersResponse = orderService.getAll();
        return new ResponseEntity<>(ordersResponse, OK);
    }

    @PreAuthorize("hasRole('ROLE_STORAGE_MANAGER')")
    public ResponseEntity<Order> updateOrderStatus(UpdateMedicineStatusRequest updateMedicineStatusRequest) {
        var order = orderService.updateStatus(updateMedicineStatusRequest);
        return new ResponseEntity<>(order, OK);
    }

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER')")
    public ResponseEntity<OrderRequest> createOrderRequestStatus(CreateOrderRequest createOrderRequest) {
        var orderRequestsResponse = orderRequestService.create(createOrderRequest);
        return new ResponseEntity<>(orderRequestsResponse, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_STORAGE_MANAGER')")
    public ResponseEntity<OrderRequest> getOrderRequestByOrderId(UUID orderId) {
        var orderRequest = orderRequestService.getByOrderId(orderId);
        return new ResponseEntity<>(orderRequest, OK);
    }

    @PreAuthorize("hasRole('ROLE_MAIN_STORAGE_MANAGER')")
    public ResponseEntity<List<OrderRequest>> getAllPendingOrderRequests() {
        var orderRequestsResponse = orderRequestService.getAllPending();
        return new ResponseEntity<>(orderRequestsResponse, OK);
    }

}
