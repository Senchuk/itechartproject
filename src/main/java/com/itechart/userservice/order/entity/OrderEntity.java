package com.itechart.userservice.order.entity;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

import java.util.UUID;
import javax.persistence.*;

import com.itechart.userservice.common.entity.AbstractEntity;
import com.itechart.userservice.order.entity.enums.MedicineEntityStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`order`")
@EqualsAndHashCode(callSuper = true)
public class OrderEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Enumerated(STRING)
    @Column(name = "medicine_status", nullable = false)
    private MedicineEntityStatus medicineStatus;

    @Column(name = "medicine_id", nullable = false)
    private UUID medicineId;

    @Column(name = "hospital_id", nullable = false)
    private UUID hospitalId;

}
