package com.itechart.userservice.order.entity;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

import java.util.UUID;
import javax.persistence.*;

import com.itechart.userservice.common.entity.AbstractEntity;
import com.itechart.userservice.order.entity.enums.OrderRequestEntityStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "order_request")
@EqualsAndHashCode(callSuper = true)
public class OrderRequestEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name="order_id", nullable=false)
    private UUID orderId;

    @Enumerated(STRING)
    @Column(name = "order_status", nullable = false)
    private OrderRequestEntityStatus orderRequestEntityStatus;

}
