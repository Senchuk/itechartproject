package com.itechart.userservice.order.entity.enums;

import javax.persistence.Table;

import lombok.Getter;

@Getter
@Table(name = "medicine_status")
public enum MedicineEntityStatus {

    PENDING,
    WHOLE,
    DAMAGED,
    LOST,
    EXPIRED

}
