package com.itechart.userservice.order.entity.enums;

import javax.persistence.Table;

import lombok.Getter;

@Getter
@Table(name = "order_request_status")
public enum OrderRequestEntityStatus {

    PENDING,
    ACCEPTED,
    REJECTED

}
