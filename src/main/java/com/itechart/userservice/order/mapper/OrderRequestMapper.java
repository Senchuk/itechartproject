package com.itechart.userservice.order.mapper;

import com.itechart.userservice.generated.model.OrderRequest;
import com.itechart.userservice.order.entity.OrderRequestEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderRequestMapper {

    OrderRequestMapper ORDER_REQUEST_MAPPER = Mappers.getMapper(OrderRequestMapper.class);

    @Mapping(source = "orderRequestStatus", target = "orderRequestEntityStatus")
    OrderRequestEntity toEntity(OrderRequest orderRequest);

    @Mapping(source = "orderRequestEntityStatus", target = "orderRequestStatus")
    OrderRequest toModel(OrderRequestEntity orderRequestEntity);

}
