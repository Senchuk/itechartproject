package com.itechart.userservice.order.mapper;

import com.itechart.userservice.generated.model.CreateOrder;
import com.itechart.userservice.generated.model.Order;
import com.itechart.userservice.order.entity.OrderEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {

    OrderMapper ORDER_MAPPER = Mappers.getMapper(OrderMapper.class);

    OrderEntity toEntity(Order order);

    @Mapping(target = "hospitalId", ignore = true)
    @Mapping(target = "medicineStatus", ignore = true)
    OrderEntity toEntity(CreateOrder createOrder);

    Order toModel(OrderEntity orderEntity);

}
