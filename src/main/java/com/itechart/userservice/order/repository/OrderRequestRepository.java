package com.itechart.userservice.order.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.order.entity.OrderRequestEntity;
import com.itechart.userservice.order.entity.enums.OrderRequestEntityStatus;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRequestRepository extends JpaRepository<OrderRequestEntity, UUID> {

    Optional<List<OrderRequestEntity>> findAllByOrderRequestEntityStatus(OrderRequestEntityStatus status);

    Optional<Integer> countOrderRequestEntitiesByOrderId(UUID orderId);

    List<OrderRequestEntity> findAllByOrderId(UUID orderId, Sort sort);

}
