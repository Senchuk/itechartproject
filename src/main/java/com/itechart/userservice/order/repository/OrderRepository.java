package com.itechart.userservice.order.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.order.entity.OrderEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, UUID> {

    Optional<OrderEntity> findByIdAndHospitalId(UUID id, UUID hospitalId);

    Optional<List<OrderEntity>> findAllByHospitalId(UUID hospitalId);

}
