package com.itechart.userservice.order.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.CreateOrderRequest;
import com.itechart.userservice.generated.model.OrderRequest;

public interface OrderRequestService {

    OrderRequest create(UUID orderId);

    OrderRequest create(CreateOrderRequest createOrderRequest);

    List<OrderRequest> getAllPending();

    OrderRequest getByOrderId(UUID orderId);

}
