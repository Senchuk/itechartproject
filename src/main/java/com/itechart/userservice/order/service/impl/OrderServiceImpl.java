package com.itechart.userservice.order.service.impl;

import static com.itechart.userservice.order.entity.enums.MedicineEntityStatus.PENDING;
import static com.itechart.userservice.order.entity.enums.MedicineEntityStatus.WHOLE;
import static com.itechart.userservice.order.mapper.OrderMapper.ORDER_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.*;
import static com.itechart.userservice.utils.SecurityUtils.getCurrentUser;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.CreateOrder;
import com.itechart.userservice.generated.model.Order;
import com.itechart.userservice.generated.model.UpdateMedicineStatusRequest;
import com.itechart.userservice.hospital.service.HospitalService;
import com.itechart.userservice.inventory.service.InventoryService;
import com.itechart.userservice.medicine.repository.MedicineRepository;
import com.itechart.userservice.order.entity.enums.MedicineEntityStatus;
import com.itechart.userservice.order.repository.OrderRepository;
import com.itechart.userservice.order.service.OrderRequestService;
import com.itechart.userservice.order.service.OrderService;
import com.itechart.userservice.storage.service.StorageService;
import com.itechart.userservice.user.service.UserService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final HospitalService hospitalService;
    private final InventoryService inventoryService;
    private final OrderRequestService orderRequestService;
    private final StorageService storageService;
    private final UserService userService;

    private final MedicineRepository medicineRepository;
    private final OrderRepository orderRepository;

    @Override
    @Transactional
    public Order create(CreateOrder createOrder) {
        medicineRepository.findById(createOrder.getMedicineId()).orElseThrow(() ->
                notFoundEntity(String.format("Medicine with such id %s was not found", createOrder.getMedicineId())));
        var orderEntity = ORDER_MAPPER.toEntity(createOrder);
        var user = userService.getCurrent();
        if (user.getStorageId() == null) {
            throw incorrectDataException(
                    String.format("Manager with such id %s does not apply to any hospital", user.getId()));
        }
        var storage = storageService.getById(user.getStorageId());
        orderEntity.setHospitalId(storage.getHospitalId());
        orderEntity.setMedicineStatus(PENDING);
        var order = orderRepository.save(orderEntity);
        orderRequestService.create(order.getId());
        return ORDER_MAPPER.toModel(order);
    }

    @Override
    @Transactional(readOnly = true)
    public Order getById(UUID id) {
        var order = orderRepository.findById(id).orElseThrow(() -> {
            throw notFoundEntity(String.format("Order with such id %s was not found", id));
        });
        return ORDER_MAPPER.toModel(order);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAll() {
        var currentUserId = getCurrentUser().getId();
        var hospitalId = hospitalService.getByManagerId(currentUserId).getId();
        var orders = orderRepository.findAllByHospitalId(hospitalId).orElseThrow(() -> {
            throw notFoundEntity(String.format("No orders with such hospital id %s were found", hospitalId));
        });
        return orders.stream().map(ORDER_MAPPER::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Order updateStatus(UpdateMedicineStatusRequest updateMedicineStatusRequest) {
        var storageId = userService.getCurrent().getStorageId();
        var hospitalId = storageService.getById(storageId).getHospitalId();
        var order = orderRepository.findByIdAndHospitalId(updateMedicineStatusRequest.getId(), hospitalId)
                .orElseThrow(() -> {
            throw notFoundEntity(String.format("Order with such id %s and hospital id %s was not found",
                    updateMedicineStatusRequest.getId(), hospitalId));
        });
        if (!order.getMedicineStatus().equals(PENDING)) {
            throw incorrectRequestException(
                    String.format("Status of the order with such id %s has been already updated", order.getId()));
        }
        var status = MedicineEntityStatus.valueOf(updateMedicineStatusRequest.getMedicineStatus().toString());
        order.setMedicineStatus(status);
        var savedOrder = orderRepository.save(order);
        if (status.equals(WHOLE)) {
            inventoryService.setMedicineAccordingToOrder(ORDER_MAPPER.toModel(savedOrder));
        }
        return ORDER_MAPPER.toModel(savedOrder);
    }

}
