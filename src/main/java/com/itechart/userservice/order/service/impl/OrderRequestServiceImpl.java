package com.itechart.userservice.order.service.impl;

import static com.itechart.userservice.generated.model.OrderRequestStatus.PENDING;
import static com.itechart.userservice.order.mapper.OrderRequestMapper.ORDER_REQUEST_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.incorrectDataException;
import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;

import static org.springframework.data.domain.Sort.Direction.DESC;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.CreateOrderRequest;
import com.itechart.userservice.generated.model.OrderRequest;
import com.itechart.userservice.inventory.service.InventoryService;
import com.itechart.userservice.order.entity.OrderRequestEntity;
import com.itechart.userservice.order.entity.enums.OrderRequestEntityStatus;
import com.itechart.userservice.order.repository.OrderRequestRepository;
import com.itechart.userservice.order.service.OrderRequestService;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderRequestServiceImpl implements OrderRequestService {

    private final InventoryService inventoryService;

    private final OrderRequestRepository orderRequestRepository;

    @Override
    @Transactional
    public OrderRequest create(UUID orderId) {
        var orderRequestEntity = new OrderRequestEntity();
        orderRequestEntity.setOrderId(orderId);
        orderRequestEntity.setOrderRequestEntityStatus(OrderRequestEntityStatus.PENDING);
        var savedOrderRequest = orderRequestRepository.save(orderRequestEntity);
        return ORDER_REQUEST_MAPPER.toModel(savedOrderRequest);
    }

    @Override
    @Transactional
    public OrderRequest create(CreateOrderRequest createOrderRequest) {
        if (createOrderRequest.getOrderRequestStatus().equals(PENDING)) {
            throw incorrectDataException(String.format("Order request status cannot be changed to %s",
                    createOrderRequest.getOrderRequestStatus()));
        }
        var amountOfOrderRequest = orderRequestRepository
                .countOrderRequestEntitiesByOrderId(createOrderRequest.getOrderId()).orElseThrow(() -> {
                    throw notFoundEntity(String.format("Order request with such order id %s was not found",
                            createOrderRequest.getOrderId()));
                });
        if (amountOfOrderRequest >= 2) {
            throw incorrectDataException(String.format("Order Request with such order id %s has already checked",
                    createOrderRequest.getOrderId()));
        }
        var newOrderRequest = new OrderRequest();
        newOrderRequest.setOrderId(createOrderRequest.getOrderId());
        newOrderRequest.setOrderRequestStatus(createOrderRequest.getOrderRequestStatus());
        var newOrderRequestEntity = ORDER_REQUEST_MAPPER.toEntity(newOrderRequest);
        var savedOrderRequest = orderRequestRepository.save(newOrderRequestEntity);
        inventoryService.getMedicineAccordingToOrder(savedOrderRequest.getOrderId());
        return ORDER_REQUEST_MAPPER.toModel(savedOrderRequest);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderRequest> getAllPending() {
        var orderRequests = orderRequestRepository.findAllByOrderRequestEntityStatus(OrderRequestEntityStatus.PENDING)
                .orElseThrow(() -> {
                    throw notFoundEntity("No order requests found");
                });
        return orderRequests.stream().map(ORDER_REQUEST_MAPPER::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public OrderRequest getByOrderId(UUID orderId) {
        var sort = Sort.by(DESC, "createdAt");
        var orderRequests = orderRequestRepository.findAllByOrderId(orderId, sort);
        if (orderRequests.isEmpty()) {
            throw notFoundEntity(String.format("There was no order request with such order id %s", orderId));
        }
        var orderRequest = orderRequests.get(0);
        return ORDER_REQUEST_MAPPER.toModel(orderRequest);
    }

}
