package com.itechart.userservice.order.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.CreateOrder;
import com.itechart.userservice.generated.model.Order;
import com.itechart.userservice.generated.model.UpdateMedicineStatusRequest;

public interface OrderService {

    Order create(CreateOrder createOrder);

    Order getById(UUID id);

    List<Order> getAll();

    Order updateStatus(UpdateMedicineStatusRequest updateMedicineStatusRequest);

}
