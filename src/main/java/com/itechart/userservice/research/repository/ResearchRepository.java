package com.itechart.userservice.research.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.itechart.userservice.research.entity.ResearchEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ResearchRepository extends JpaRepository<ResearchEntity, UUID> {

    Boolean existsByResearcherId(UUID researcherId);

    Optional<ResearchEntity> findByResearcherId(UUID researcherId);

    @Query("SELECT research " +
           "FROM ResearchEntity AS research " +
           "WHERE research.id IN (:ids)")
    List<ResearchEntity> findAllById(List<UUID> ids);

}
