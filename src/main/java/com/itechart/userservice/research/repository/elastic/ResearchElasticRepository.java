package com.itechart.userservice.research.repository.elastic;

import java.util.UUID;

import com.itechart.userservice.research.document.ResearchDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResearchElasticRepository extends ElasticsearchRepository<ResearchDocument, UUID> {

    @Query("{" +
           " \"match\" : { " +
           "  \"name\" : \"?0\"" +
           " }" +
           "}")
    Page<ResearchDocument> findAll(String search,
                                   Pageable pageable);

    Page<ResearchDocument> findAll(Pageable pageable);

}
