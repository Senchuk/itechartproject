package com.itechart.userservice.research.api;

import static com.itechart.userservice.utils.HeaderUtils.generatePaginationHeaders;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.api.ResearchesApi;
import com.itechart.userservice.generated.model.Research;
import com.itechart.userservice.research.service.ResearchService;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class ResearchesApiImpl implements ResearchesApi {

    private final ResearchService researchService;

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Research> createResearch(Research research) {
        var researchResponse = researchService.create(research);
        return new ResponseEntity<>(researchResponse, CREATED);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER')")
    public ResponseEntity<Research> getResearchByResearcherId(UUID id) {
        var researchResponse = researchService.getByResearcherId(id);
        return new ResponseEntity<>(researchResponse, OK);
    }

    @PreAuthorize("hasRole('ROLE_RESEARCHER') or hasRole('ROLE_SPONSOR')")
    public ResponseEntity<List<Research>> getAllResearch(Integer page, Integer size,
                                                         List<String> sort, String search) {
        var research = researchService.getPage(page, size, sort, search);
        var headers = generatePaginationHeaders(research);
        return new ResponseEntity<>(research, headers, OK);
    }

}
