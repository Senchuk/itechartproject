package com.itechart.userservice.research.document;

import static org.springframework.data.elasticsearch.annotations.FieldType.Text;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import lombok.Data;

@Data
@Document(indexName = "research", type = "_doc")
public class ResearchDocument {

    @Id
    private UUID id;

    @Field(type = Text, fielddata = true)
    private String name;

    private UUID hospitalId;

    private UUID researcherId;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

}
