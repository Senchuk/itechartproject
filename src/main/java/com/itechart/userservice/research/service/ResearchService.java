package com.itechart.userservice.research.service;

import java.util.List;
import java.util.UUID;

import com.itechart.userservice.generated.model.Research;

public interface ResearchService {

    Research create(Research research);

    Research getByResearcherId(UUID id);

    List<Research> getPage(Integer page, Integer size, List<String> sort, String search);

}
