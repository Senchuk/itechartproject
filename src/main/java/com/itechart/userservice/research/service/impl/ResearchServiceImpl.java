package com.itechart.userservice.research.service.impl;

import static java.util.Collections.emptyList;

import static com.itechart.userservice.generated.model.UserScope.RESEARCHER;
import static com.itechart.userservice.research.mapper.ResearchMapper.RESEARCH_MAPPER;
import static com.itechart.userservice.utils.ExceptionUtils.incorrectDataException;
import static com.itechart.userservice.utils.ExceptionUtils.notFoundEntity;
import static com.itechart.userservice.utils.PagingUtils.toPageable;
import static com.itechart.userservice.utils.SecurityUtils.getCurrentUser;
import static com.itechart.userservice.utils.SortUtils.parseSortOrDefault;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.itechart.userservice.generated.model.Research;
import com.itechart.userservice.hospital.repository.HospitalRepository;
import com.itechart.userservice.research.document.ResearchDocument;
import com.itechart.userservice.research.repository.ResearchRepository;
import com.itechart.userservice.research.repository.elastic.ResearchElasticRepository;
import com.itechart.userservice.research.service.ResearchService;
import com.itechart.userservice.user.service.UserService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ResearchServiceImpl implements ResearchService {

    private final UserService userService;

    private final HospitalRepository hospitalRepository;
    private final ResearchRepository researchRepository;
    private final ResearchElasticRepository researchElasticRepository;

    @Override
    @Transactional
    public Research create(Research research) {
        var isExists = hospitalRepository.existsById(research.getHospitalId());
        if (!isExists) {
            throw notFoundEntity(String.format("Hospital with such id %s was not found", research.getHospitalId()));
        }
        var researchEntity = RESEARCH_MAPPER.toEntity(research);
        var id = getCurrentUser().getId();
        var isResearcherBusy = researchRepository.existsByResearcherId(id);
        if (isResearcherBusy) {
            throw incorrectDataException(
                    String.format("Researcher with such id %s already refers to another research", id));
        }
        researchEntity.setResearcherId(id);
        var savedResearch = researchRepository.save(researchEntity);
        var researchDocument = RESEARCH_MAPPER.toDocument(savedResearch);
        researchElasticRepository.save(researchDocument);
        return RESEARCH_MAPPER.toModel(savedResearch);
    }

    @Override
    @Transactional(readOnly = true)
    public Research getByResearcherId(UUID id) {
        var researcher = userService.getById(id);
        if (researcher.getScope() == RESEARCHER) {
            return researchRepository.findByResearcherId(id).map(RESEARCH_MAPPER::toModel).orElseThrow(() -> {
                throw notFoundEntity(String.format("Research with such researcher id %s was not found", id));
            });
        }
        throw incorrectDataException(
                String.format("User with such id %s does not have access", id));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Research> getPage(Integer page, Integer size, List<String> sort, String search) {
        var sorting = parseSortOrDefault(sort, ResearchDocument.class);
        var pageable = toPageable(page, size, sorting);
        var researchDocuments = getDocumentsPage(search, pageable);
        if (researchDocuments.isEmpty()) {
            return emptyList();
        }
        var ids = researchDocuments.stream().map(ResearchDocument::getId).collect(Collectors.toList());
        var researches = researchRepository.findAllById(ids);
        return researches.stream().map(RESEARCH_MAPPER::toModel).collect(Collectors.toList());
    }

    private Page<ResearchDocument> getDocumentsPage(String search, Pageable pageable) {
        if (search == null) {
            return researchElasticRepository.findAll(pageable);
        }
        return researchElasticRepository.findAll(search, pageable);
    }

}
