package com.itechart.userservice.research.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.UUID;
import javax.persistence.*;

import com.itechart.userservice.common.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "research")
@EqualsAndHashCode(callSuper = true)
public class ResearchEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "hospital_id", nullable = false)
    private UUID hospitalId;

    @Column(name = "researcher_id", nullable = false)
    private UUID researcherId;

}
