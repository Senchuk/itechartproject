package com.itechart.userservice.research.mapper;

import com.itechart.userservice.generated.model.Research;
import com.itechart.userservice.research.document.ResearchDocument;
import com.itechart.userservice.research.entity.ResearchEntity;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ResearchMapper {

    ResearchMapper RESEARCH_MAPPER = Mappers.getMapper(ResearchMapper.class);

    ResearchEntity toEntity(Research research);

    Research toModel(ResearchEntity researchEntity);

    ResearchDocument toDocument(ResearchEntity researchEntity);

}
