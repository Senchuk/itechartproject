package com.itechart.userservice.integration.visitsApiTest;

import static com.itechart.userservice.generated.model.MedicineStatus.WHOLE;
import static com.itechart.userservice.generated.model.MedicineType.CAPSULE;
import static com.itechart.userservice.generated.model.OrderRequestStatus.ACCEPTED;
import static com.itechart.userservice.generated.model.PatientGender.FEMALE;
import static com.itechart.userservice.generated.model.PatientStatus.SCREENED;
import static com.itechart.userservice.generated.model.UserScope.*;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.Visit;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class VisitsApiImplTest extends AbstractIntegrationTest {

    private static final String VISITS_URL = BASE_URL + "/visits";

    //region create patient visit tests

    @Test
    public void createPatientVisitWithoutTheDistributionOfMedicine_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 10);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);
        createOrderRequest(orderRequest, mainManagerSecondTokenResponse.getAccessToken());

        var managerSecondTokenResponse = signIn(managerSignInRequest);

        var updateMedicineStatus = updateMedicineStatusRequest(savedOrder.getId(), WHOLE);
        updateMedicineStatus(updateMedicineStatus, managerSecondTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "researcher@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest("researcher@mail.com", "Password123");
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var research = research("test", savedHospital.getId());
        createResearch(research, researcherTokenResponse.getAccessToken());

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        var savedPatient = createPatient(patient, researcherTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(post(VISITS_URL + "/" + savedPatient.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()))
                        .andExpect(status().isCreated())
                        .andReturn();

        final Visit response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Visit>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getDate()).isNotNull();
            softly.assertThat(response.getMedicineId()).isNull();
            softly.assertThat(response.getPatientId()).isEqualTo(savedPatient.getId());
        });
    }

    @Test
    public void createPatientVisitWithTheDistributionOfMedicine_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 10);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);
        createOrderRequest(orderRequest, mainManagerSecondTokenResponse.getAccessToken());

        var managerSecondTokenResponse = signIn(managerSignInRequest);

        var updateMedicineStatus = updateMedicineStatusRequest(savedOrder.getId(), WHOLE);
        updateMedicineStatus(updateMedicineStatus, managerSecondTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "researcher@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest("researcher@mail.com", "Password123");
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var research = research("test", savedHospital.getId());
        createResearch(research, researcherTokenResponse.getAccessToken());

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        var savedPatient = createPatient(patient, researcherTokenResponse.getAccessToken());

        createVisit(savedPatient.getId(), researcherTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(post(VISITS_URL + "/" + savedPatient.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()))
                        .andExpect(status().isCreated())
                        .andReturn();

        final Visit response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Visit>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getDate()).isNotNull();
            softly.assertThat(response.getMedicineId()).isNotNull();
            softly.assertThat(response.getPatientId()).isEqualTo(savedPatient.getId());
        });
    }

    //endregion

}
