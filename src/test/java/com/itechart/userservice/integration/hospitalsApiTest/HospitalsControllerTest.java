package com.itechart.userservice.integration.hospitalsApiTest;

import static com.itechart.userservice.generated.model.UserScope.SPONSOR;
import static com.itechart.userservice.generated.model.UserScope.STORAGE_MANAGER;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.Hospital;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class HospitalsControllerTest extends AbstractIntegrationTest {

    private static final String HOSPITALS_URL = BASE_URL + "/hospitals";

    //region create hospital tests

    @Test
    public void createHospital_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                            "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());

        //WHEN
        var mvcResult =
                mockMvc.perform(post(HOSPITALS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(hospital)))
                        .andExpect(status().isCreated())
                        .andReturn();

        final Hospital response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Hospital>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getName()).isEqualTo(hospital.getName());
            softly.assertThat(response.getAddress()).isEqualTo(hospital.getAddress());
            softly.assertThat(response.getManagerId()).isEqualTo(hospital.getManagerId());
        });
    }

    @Test
    public void createHospitalWhenUserIsNotManager() throws Exception {
        //GIVEN
        var firstSponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(firstSponsorSignUpRequest);
        var firstSponsorSignInRequest = createSignInRequest(firstSponsorSignUpRequest.getEmail(), firstSponsorSignUpRequest.getPassword());
        var firstSponsorTokenResponse = signIn(firstSponsorSignInRequest);

        var secondSponsorSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", SPONSOR);
        var savedSecondSponsor = singUpUser(secondSponsorSignUpRequest);

        var hospital = hospital("test", "test", savedSecondSponsor.getId());

        //WHEN
        var resultActions =
                mockMvc.perform(post(HOSPITALS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + firstSponsorTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(hospital)));

        //THEN
       resultActions.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void createHospitalWhenManagerAlreadyTaken() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        //WHEN
        var resultActions =
                mockMvc.perform(post(HOSPITALS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(hospital)));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    //endregion

    //region get all hospitals tests

    @Test
    public void getAllHospitals_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(get(HOSPITALS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(hospital)))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Hospital> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Hospital>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.size()).isEqualTo(1);
            softly.assertThat(response.get(0).getId()).isNotNull();
            softly.assertThat(response.get(0).getName()).isEqualTo(hospital.getName());
            softly.assertThat(response.get(0).getAddress()).isEqualTo(hospital.getAddress());
            softly.assertThat(response.get(0).getManagerId()).isEqualTo(hospital.getManagerId());

        });
    }

    //endregion

}
