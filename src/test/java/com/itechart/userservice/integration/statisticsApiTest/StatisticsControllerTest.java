package com.itechart.userservice.integration.statisticsApiTest;

import static com.itechart.userservice.generated.model.MedicineStatus.WHOLE;
import static com.itechart.userservice.generated.model.MedicineType.CAPSULE;
import static com.itechart.userservice.generated.model.OrderRequestStatus.ACCEPTED;
import static com.itechart.userservice.generated.model.PatientGender.FEMALE;
import static com.itechart.userservice.generated.model.PatientStatus.SCREENED;
import static com.itechart.userservice.generated.model.UserScope.*;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.Statistics;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class StatisticsControllerTest extends AbstractIntegrationTest {

    private static final String STATISTICS_URL = BASE_URL + "/statistics";

    //region collect statistics tests

    @Test
    public void collectStatistics_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 10);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInSRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInSRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);
        createOrderRequest(orderRequest, mainManagerSecondTokenResponse.getAccessToken());

        var managerSecondTokenResponse = signIn(managerSignInRequest);

        var updateMedicineStatus = updateMedicineStatusRequest(savedOrder.getId(), WHOLE);
        updateMedicineStatus(updateMedicineStatus, managerSecondTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "researcher@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest("researcher@mail.com", "Password123");
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var research = research("test", savedHospital.getId());
        createResearch(research, researcherTokenResponse.getAccessToken());

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        var savedPatient = createPatient(patient, researcherTokenResponse.getAccessToken());

        createVisit(savedPatient.getId(), researcherTokenResponse.getAccessToken());
        createVisit(savedPatient.getId(), researcherTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(get(STATISTICS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final Statistics response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Statistics>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getPatientQuantity()).isEqualTo(1);
            softly.assertThat(response.getVisitQuantity()).isEqualTo(2);
            softly.assertThat(response.getDistributedMedicineQuantity()).isEqualTo(1);
        });
    }

    //endregion

}
