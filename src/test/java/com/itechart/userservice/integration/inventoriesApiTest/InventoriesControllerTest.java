package com.itechart.userservice.integration.inventoriesApiTest;

import static com.itechart.userservice.generated.model.MedicineType.CAPSULE;
import static com.itechart.userservice.generated.model.UserScope.SPONSOR;
import static com.itechart.userservice.generated.model.UserScope.STORAGE_MANAGER;
import static com.itechart.userservice.medicine.entity.enums.MedicineTypeEntity.MIXTURE;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.CreateInventoryRequest;
import com.itechart.userservice.generated.model.Inventory;
import com.itechart.userservice.integration.AbstractIntegrationTest;
import com.itechart.userservice.inventory.service.schedule.InventoryScheduleService;
import com.itechart.userservice.medicine.document.enums.MedicineTypeDocument;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;

public class InventoriesControllerTest extends AbstractIntegrationTest {

    private static final String INVENTORIES_URL = BASE_URL + "/inventories";

    @Autowired
    private InventoryScheduleService inventoryScheduleService;

    //region create inventory tests

    @Test
    public void createInventory_happyPath() throws Exception {
        //GIVEN
        var mainManagerToSignIn = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerToSignIn);

        var medicine = medicine("test", "test", "test",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);

        //WHEN
        var mvcResult =
                mockMvc.perform(post(INVENTORIES_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(inventory)))
                        .andExpect(status().isCreated())
                        .andReturn();

        final CreateInventoryRequest response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<CreateInventoryRequest>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getOrderId()).isNull();
            softly.assertThat(response.getStorageId().toString()).isEqualTo("14142e8c-b76e-4d0d-8370-8769de25ceb3");
            softly.assertThat(response.getMedicineId()).isEqualTo(savedMedicine.getId());
            softly.assertThat(response.getQuantity()).isEqualTo(inventory.getQuantity());
        });
    }

    //endregion

    //region get available medicine quantity tests

    @Test
    public void getAvailableMedicinesQuantity_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        //WHEN
        var mvcResult =
                mockMvc.perform(get(INVENTORIES_URL + "/medicines/available/quantity/" + savedMedicine.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerSecondTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final Integer response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Integer>() {
                });

        //THEN
        assertThat(response).isEqualTo(5);
    }

    //endregion

    //region get all inventory tests

    @Test
    public void getAllInventories_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var sort = new String[]{"+quantity"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(INVENTORIES_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("isLocalStorage", "true")
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Inventory> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Inventory>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(1);
    }

    //endregion

    //region delete inventory tests

    @Test
    public void deleteInventory_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        var savedInventory = createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        //WHEN
        var resultActions =
                mockMvc.perform(delete(INVENTORIES_URL + "/" + savedInventory.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNoContent());
    }

    //endregion

    //region delete inventory tests

    @Test
    public void expirationDateCheck_happyPath() {
        //GIVEN
        var medicineId = UUID.randomUUID();
        var inventoryId = UUID.randomUUID();
        var orderId = UUID.randomUUID();
        var storageId = UUID.randomUUID();
        var medicineExpirationDate = Date.from(Instant.now().minusSeconds(99999));
        setMedicineIntoDatabase(medicineId, "test", "test", "test",
                MIXTURE, medicineExpirationDate);
        var medicine = setMedicineIntoElastic(medicineId, "test", "test", "test",
                MedicineTypeDocument.MIXTURE, medicineExpirationDate);
        setInventoryIntoDatabase(inventoryId, UUID.randomUUID(), medicineId, 5, UUID.randomUUID());
        setInventoryIntoElastic(inventoryId, storageId, medicine, 5, orderId);

        //WHEN
        inventoryScheduleService.deleteExpiredMedicinesFromInventories();

        var medicinesDeletedAt = getMedicineDeletedAtById(medicineId);
        var inventoryDeletedAt = getInventoryDeletedAtByMedicineId(medicineId);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(medicinesDeletedAt).isNotNull();
            softly.assertThat(inventoryDeletedAt).isNotNull();
        });

    }

    //endregion

}
