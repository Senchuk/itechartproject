package com.itechart.userservice.integration.usersApiTest;

import static com.itechart.userservice.generated.model.UserScope.*;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class UsersControllerTest extends AbstractIntegrationTest {

    private static final String USER_URL = BASE_URL + "/users";

    //region pagination tests

    @Test
    public void getAllUsers_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                            "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test2@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);

        var sort = new String[]{"+firstName", "-lastName"};
        var filterScope = new String[]{"ROLE_SPONSOR"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(USER_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("scopes", filterScope)
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<User> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<User>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(1);
    }

    @Test
    public void getEmptyUsersPage() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var sort = new String[]{"+firstName", "-lastName"};
        var filterScope = new String[]{"ROLE_RESEARCHER"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(USER_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .param("page", "2")
                        .param("size", "1")
                        .param("sort", sort)
                        .param("scopes", filterScope)
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<User> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<User>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(0);
    }

    @Test
    public void getAllUsersWithoutFilter() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test2@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);

        var sort = new String[]{"+firstName", "-lastName"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(USER_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<User> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<User>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(2);
    }

    @Test
    public void getAllUsersWithoutSearch() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test2@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);

        var sort = new String[]{"+firstName", "-lastName"};
        var filterScope = new String[]{"ROLE_SPONSOR"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(USER_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("scopes", filterScope))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<User> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<User>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(1);
    }

    @Test
    public void getAllBlockedUsers() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test2@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);

        var sort = new String[]{"+firstName", "-lastName"};
        var filterScope = new String[]{"ROLE_SPONSOR"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(USER_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("scopes", filterScope)
                        .param("isBlocked", "true")
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<User> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<User>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(0);
    }

    @Test
    public void getAllNotBlockedUsersWithoutSearch() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test2@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);

        var sort = new String[]{"+firstName", "-lastName"};
        var filterScope = new String[]{"ROLE_SPONSOR"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(USER_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("scopes", filterScope)
                        .param("isBlocked", "false"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<User> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<User>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(1);
    }

    //endregion

    //region update user personal data tests

    @Test
    public void updateUser_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        var savedSponsor = singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var sponsorToUpdate = createUser(savedSponsor.getId(), "test", "test",
                "toUpdate@mail.com", SPONSOR);

        //WHEN
        var mvcResult =
                mockMvc.perform(put(USER_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(sponsorToUpdate)))
                        .andExpect(status().isOk())
                        .andReturn();

        var response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getId()).isEqualTo(savedSponsor.getId());
            softly.assertThat(response.getEmail()).isEqualTo(sponsorToUpdate.getEmail());
        });
    }

    //endregion

    //region change user password tests

    @Test
    public void changePassword_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var changePasswordRequest = createChangePasswordRequest("Password123", "NewPassword123");

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + "/changePassword")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(changePasswordRequest)));

        //THEN
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void changePasswordWhileInputtedOldPasswordIncorrect() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var changePasswordRequest = createChangePasswordRequest("12345678","NewPassword123");

        //WHEN
        var resultActions = mockMvc.perform(put(USER_URL + "/changePassword")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(changePasswordRequest)));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    //endregion

    //region block user tests

    @Test
    public void blockUser_happyPath() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(),
                researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "toBlock@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/" + savedManager.getId()+ "/block")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isOk());
    }


    @Test
    public void blockUserWhichHasAlreadyBlocked() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(),
                researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "toBlock@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        blockUser(savedManager.getId(), researcherTokenResponse.getAccessToken());

        //WHEN
        var resultActions = mockMvc.perform(get(USER_URL + "/" + savedManager.getId() +  "/block")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    //endregion

    //region delete user tests

    @Test
    public void deleteUser_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "toDelete@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        //WHEN
        var resultActions = mockMvc.perform(delete(USER_URL + "/" + savedManager.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNoContent());
    }

    @Test
    public void deleteUserWhichWasDeleted() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "toDelete@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        deleteUser(savedManager.getId(), sponsorTokenResponse.getAccessToken());

        //WHEN
        var resultActions = mockMvc.perform(delete(USER_URL + "/" + savedManager.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    //endregion

    //region get all by user scope tests

    @Test
    public void getAllUsersByUserScope_happyPath() throws Exception {
        //GIVEN
        var firstSponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(firstSponsorSignUpRequest);
        var secondSponsorSignUpRequest = createSignUpRequest("test", "test", "test1@mail.com",
                "Password123", SPONSOR);
        singUpUser(secondSponsorSignUpRequest);
        var firstSponsorSignInRequest = createSignInRequest(firstSponsorSignUpRequest.getEmail(),
                firstSponsorSignUpRequest.getPassword());
        var firstSponsorTokenResponse = signIn(firstSponsorSignInRequest);

        //WHEN
        var mvcResult =
                mockMvc.perform(get(USER_URL + "/all/" + "SPONSOR")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + firstSponsorTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<User> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<User>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.size()).isEqualTo(2);
        });
    }

    @Test
    public void getAllUsersByUserScopeWhenUsersNotFound() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(),
                sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        //WHEN
        var resultActions =
                mockMvc.perform(get(USER_URL + "/all/" + "RESEARCHER")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    //endregion

}

