package com.itechart.userservice.integration.authApiTest;

import com.itechart.userservice.generated.model.TokenResponse;
import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.integration.AbstractIntegrationTest;
import org.junit.Test;

import static com.itechart.userservice.generated.model.UserScope.MAIN_STORAGE_MANAGER;
import static com.itechart.userservice.generated.model.UserScope.SPONSOR;
import static com.itechart.userservice.utils.TestUtils.createSignInRequest;
import static com.itechart.userservice.utils.TestUtils.createSignUpRequest;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthControllerTest extends AbstractIntegrationTest {

    private static final String AUTH_URL = BASE_URL + "/auth";

    //region signUp tests

    @Test
    public void signUpUser_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);

        //WHEN
        var mvcResult =
                mockMvc.perform(post(AUTH_URL + "/signup")
                        .content(objectMapper.writeValueAsString(sponsorSignUpRequest))
                        .contentType(APPLICATION_JSON))
                        .andExpect(status().isCreated())
                        .andReturn();

        var response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getEmail()).isEqualTo(sponsorSignUpRequest.getEmail());
            softly.assertThat(response.getFirstName()).isEqualTo(sponsorSignUpRequest.getFirstName());
            softly.assertThat(response.getLastName()).isEqualTo(sponsorSignUpRequest.getLastName());
            softly.assertThat(response.getScope()).isEqualTo(sponsorSignUpRequest.getScope());
        });
    }

    @Test
    public void signUpUserWithEmailThatBelongsToAnotherUser() throws Exception {
        //GIVEN
        var firstSponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(firstSponsorSignUpRequest);
        var secondSponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(secondSponsorSignUpRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void signUpUserWithScopeMainStorageManager() throws Exception {
        //GIVEN
        var managerSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", MAIN_STORAGE_MANAGER);

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(managerSignUpRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    //endregion

    //region signIn tests

    @Test
    public void signInUser_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest("test@mail.com", "Password123");

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(sponsorSignInRequest))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        var tokenResponse = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), TokenResponse.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(tokenResponse.getAccessToken()).isNotNull();
            softly.assertThat(tokenResponse.getAccessTokenExpirationTime()).isNotNull();
            softly.assertThat(tokenResponse.getRefreshToken()).isNotNull();
            softly.assertThat(tokenResponse.getRefreshTokenExpirationTime()).isNotNull();
        });
    }

    @Test
    public void signInUserWithEmailThatNotExists() throws Exception {
        //GIVEN
        var userSignInRequest = createSignInRequest("test1@mail.com", "Password123");

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(userSignInRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void signInUserWithIncorrectPassword() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest("test@mail.com", "Password111");

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(sponsorSignInRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    //endregion

}

