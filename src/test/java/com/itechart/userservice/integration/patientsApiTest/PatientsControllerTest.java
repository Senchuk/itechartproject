package com.itechart.userservice.integration.patientsApiTest;

import static com.itechart.userservice.generated.model.PatientGender.FEMALE;
import static com.itechart.userservice.generated.model.PatientStatus.ACCEPTED_INTO_THE_RESEARCHING;
import static com.itechart.userservice.generated.model.PatientStatus.SCREENED;
import static com.itechart.userservice.generated.model.UserScope.RESEARCHER;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.Patient;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class PatientsControllerTest extends AbstractIntegrationTest {

    private static final String PATIENT_URL = BASE_URL + "/patients";

    //region create patient tests

    @Test
    public void createPatient_happyPath() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                            "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);

        //WHEN
        var mvcResult =
                mockMvc.perform(post(PATIENT_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(patient)))
                        .andExpect(status().isCreated())
                        .andReturn();

        final Patient response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Patient>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getResearcherId()).isNotNull();
            softly.assertThat(response.getCreatedAt()).isNotNull();
            softly.assertThat(response.getFirstName()).isEqualTo(patient.getFirstName());
            softly.assertThat(response.getLastName()).isEqualTo(patient.getLastName());
            softly.assertThat(response.getSecondName()).isEqualTo(patient.getSecondName());
            softly.assertThat(response.getGender()).isEqualTo(patient.getGender());
            softly.assertThat(response.getStatus()).isEqualTo(patient.getStatus());
            softly.assertThat(response.getDob()).isEqualTo(patient.getDob());
        });
    }

    //endregion

    //region patient pagination tests

    @Test
    public void getAllPatients_happyPath() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var firstPatient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        createPatient(firstPatient, researcherTokenResponse.getAccessToken());

        var secondPatient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        createPatient(secondPatient, researcherTokenResponse.getAccessToken());

        var sort = new String[]{"+firstName", "-lastName"};
        var filter = new String[]{"SCREENED"};
        var search = "testPatient";

        //WHEN
        var mvcResult =
                mockMvc.perform(get(PATIENT_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("statuses", filter)
                        .param("search", search))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Patient> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Patient>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(2);
    }

    @Test
    public void getEmptyPatientsPage() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        createPatient(patient, researcherTokenResponse.getAccessToken());

        var sort = new String[]{"+firstName", "-lastName"};
        var filter = new String[]{"SCREENED"};
        var search = "testPatient";

        //WHEN
        var mvcResult =
                mockMvc.perform(get(PATIENT_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .param("page", "2")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("statuses", filter)
                        .param("search", search))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Patient> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Patient>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(0);
    }

    @Test
    public void getAllPatientsWithoutFilter() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var firstPatient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        createPatient(firstPatient, researcherTokenResponse.getAccessToken());

        var secondPatient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        createPatient(secondPatient, researcherTokenResponse.getAccessToken());

        var sort = new String[]{"+firstName", "-lastName"};
        var search = "testPatient";

        //WHEN
        var mvcResult =
                mockMvc.perform(get(PATIENT_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("search", search))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Patient> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Patient>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(2);
    }

    @Test
    public void getAllPatientsWithoutSearch() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        createPatient(patient, researcherTokenResponse.getAccessToken());

        var sort = new String[]{"+firstName", "-lastName"};
        var filter = new String[]{"ACCEPTED_INTO_THE_RESEARCHING"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(PATIENT_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("statuses", filter))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Patient> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Patient>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(0);
    }

    //endregion

    //region get by id tests

    @Test
    public void getPatientById_happyPath() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        var savedPatient = createPatient(patient, researcherTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(get(PATIENT_URL + "/" + savedPatient.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final Patient response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Patient>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getId()).isEqualTo(savedPatient.getId());
            softly.assertThat(response.getFirstName()).isEqualTo(savedPatient.getLastName());
            softly.assertThat(response.getLastName()).isEqualTo(savedPatient.getLastName());
            softly.assertThat(response.getSecondName()).isEqualTo(savedPatient.getSecondName());
            softly.assertThat(response.getGender()).isEqualTo(savedPatient.getGender());
            softly.assertThat(response.getStatus()).isEqualTo(savedPatient.getStatus());
            softly.assertThat(response.getResearcherId()).isEqualTo(savedPatient.getResearcherId());
        });
    }

    @Test
    public void getPatientByIdWhenPatientNotFound() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        //WHEN
        var resultActions =
                mockMvc.perform(get(PATIENT_URL + "/" + UUID.randomUUID())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    //endregion

    //region update patient status tests

    @Test
    public void updatePatientStatus_happyPath() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var patient = patient("testPatient", "testPatient", "testPatient",
                FEMALE, Date.from(Instant.now()), SCREENED);
        var savedPatient = createPatient(patient, researcherTokenResponse.getAccessToken());

        var updateStatusRequest = createChangeStatusRequest(ACCEPTED_INTO_THE_RESEARCHING);

        //WHEN
        var mvcResult =
                mockMvc.perform(put(PATIENT_URL + "/" + savedPatient.getId() + "/status")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(updateStatusRequest)))
                        .andExpect(status().isOk())
                        .andReturn();

        final Patient response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Patient>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getStatus()).isEqualTo(updateStatusRequest.getStatus());
        });
    }

    //endregion

}
