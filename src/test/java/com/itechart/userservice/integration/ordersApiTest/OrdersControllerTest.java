package com.itechart.userservice.integration.ordersApiTest;

import static com.itechart.userservice.generated.model.MedicineStatus.WHOLE;
import static com.itechart.userservice.generated.model.MedicineType.CAPSULE;
import static com.itechart.userservice.generated.model.OrderRequestStatus.ACCEPTED;
import static com.itechart.userservice.generated.model.UserScope.SPONSOR;
import static com.itechart.userservice.generated.model.UserScope.STORAGE_MANAGER;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.Inventory;
import com.itechart.userservice.generated.model.Order;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class OrdersControllerTest extends AbstractIntegrationTest {

    private static final String ORDERS_URL = BASE_URL + "/orders";

    //region create order request tests

    @Test
    public void createOrderRequest_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var mmanagerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(mmanagerSignInRequest);

        var order = order(savedMedicine.getId(), 5);

        //WHEN
        var mvcResult =
                mockMvc.perform(post(ORDERS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + managerTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(order)))
                        .andExpect(status().isCreated())
                        .andReturn();

        final Inventory response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Inventory>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
        });
    }

    //endregion

    //region get all orders tests

    @Test
    public void getAllOrders_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(get(ORDERS_URL + "/all")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + managerTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Order> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Order>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.size()).isEqualTo(1);
            softly.assertThat(response.get(0).getId()).isEqualTo(savedOrder.getId());
            softly.assertThat(response.get(0).getHospitalId()).isEqualTo(savedOrder.getHospitalId());
            softly.assertThat(response.get(0).getMedicineId()).isEqualTo(savedOrder.getMedicineId());
            softly.assertThat(response.get(0).getMedicineStatus()).isEqualTo(savedOrder.getMedicineStatus());
            softly.assertThat(response.get(0).getQuantity()).isEqualTo(savedOrder.getQuantity());
        });
    }

    //endregion

    //region update medicine status tests

    @Test
    public void updateMedicineStatus_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);
        createOrderRequest(orderRequest, mainManagerSecondTokenResponse.getAccessToken());

        var managerSecondTokenResponse = signIn(managerSignInRequest);

        var updateMedicineStatusRequest = updateMedicineStatusRequest(savedOrder.getId(), WHOLE);

        //WHEN
        var mvcResult =
                mockMvc.perform(put(ORDERS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + managerSecondTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(updateMedicineStatusRequest)))
                        .andExpect(status().isOk())
                        .andReturn();

        final Order response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Order>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getHospitalId()).isEqualTo(savedOrder.getHospitalId());
            softly.assertThat(response.getMedicineId()).isEqualTo(savedOrder.getMedicineId());
            softly.assertThat(response.getQuantity()).isEqualTo(savedOrder.getQuantity());
            softly.assertThat(response.getMedicineStatus()).isEqualTo(updateMedicineStatusRequest.getMedicineStatus());
        });
    }

    //endregion

}
