package com.itechart.userservice.integration.orderRequestsApiTest;

import static com.itechart.userservice.generated.model.MedicineType.CAPSULE;
import static com.itechart.userservice.generated.model.OrderRequestStatus.*;
import static com.itechart.userservice.generated.model.UserScope.SPONSOR;
import static com.itechart.userservice.generated.model.UserScope.STORAGE_MANAGER;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.OrderRequest;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class OrderRequestsControllerTest extends AbstractIntegrationTest {

    private static final String ORDERS_REQUESTS_URL = BASE_URL + "/orders/requests";

    //region create order request tests

    @Test
    public void createOrderRequest_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);
        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);

        //WHEN
        var mvcResult =
                mockMvc.perform(post(ORDERS_REQUESTS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerSecondTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(orderRequest)))
                        .andExpect(status().isCreated())
                        .andReturn();

        final OrderRequest response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<OrderRequest>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getOrderId()).isEqualTo(savedOrder.getId());
            softly.assertThat(response.getOrderRequestStatus()).isEqualTo(orderRequest.getOrderRequestStatus());
        });
    }

    @Test
    public void createOrderRequestWhenOrderRequestHasAlreadyChecked() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);
        createOrderRequest(orderRequest, mainManagerSecondTokenResponse.getAccessToken());

        var newOrderRequest = orderRequest(savedOrder.getId(), REJECTED);

        //WHEN
        var resultActions =
                mockMvc.perform(post(ORDERS_REQUESTS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerSecondTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(newOrderRequest)));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void createOrderRequestWhenOrderRequestStatusIsPending() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), PENDING);

        //WHEN
        var resultActions =
                mockMvc.perform(post(ORDERS_REQUESTS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerSecondTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(orderRequest)));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void createOrderRequestWhenAvailableMedicineIsNotExists() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);

        //WHEN
        var resultActions =
                mockMvc.perform(post(ORDERS_REQUESTS_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerSecondTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(orderRequest)));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    //endregion

    //region get all pending order requests tests

    @Test
    public void getAllOrderRequests_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInSRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInSRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        //WHEN
        var mvcResult =
                mockMvc.perform(get(ORDERS_REQUESTS_URL + "/all")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerSecondTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(order)))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<OrderRequest> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<OrderRequest>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.size()).isEqualTo(1);
            softly.assertThat(response.get(0).getOrderId()).isEqualTo(savedOrder.getId());
            softly.assertThat(response.get(0).getOrderRequestStatus()).isEqualTo(PENDING);
        });
    }

    //endregion

    //region get order request tests

    @Test
    public void getOrderRequest_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 5);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);
        var savedOrderRequest = createOrderRequest(orderRequest, mainManagerSecondTokenResponse.getAccessToken());

        var ManagerSecondTokenResponse = signIn(managerSignInRequest);

        //WHEN
        var mvcResult =
                mockMvc.perform(get(BASE_URL + "/orders/" + savedOrder.getId() + "/requests")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + ManagerSecondTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final OrderRequest response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<OrderRequest>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getId()).isEqualTo(savedOrderRequest.getId());
            softly.assertThat(response.getOrderId()).isEqualTo(savedOrderRequest.getOrderId());
            softly.assertThat(response.getOrderRequestStatus()).isEqualTo(savedOrderRequest.getOrderRequestStatus());
        });
    }

    @Test
    public void getOrderRequestWhenOrderRequestWasNotFound() throws Exception {
        //GIVEN

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        singUpUser(managerSignUpRequest);
        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        //WHEN
        var resultActions =
                mockMvc.perform(get(BASE_URL + "/orders/" + UUID.randomUUID() + "/requests")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + managerTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    //endregion

}
