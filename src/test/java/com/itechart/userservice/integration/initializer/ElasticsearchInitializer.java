package com.itechart.userservice.integration.initializer;

import org.jetbrains.annotations.NotNull;

import org.junit.ClassRule;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class ElasticsearchInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext>  {

    private static final int ELASTIC_PORT = 9300;

    private static final String ELASTIC_CLUSTER_NAME = "user-service-cluster";

    @ClassRule
    private static final GenericContainer ELASTICSEARCH =
            new GenericContainer("elasticsearch:6.8.3")
                    .waitingFor(Wait.forListeningPort())
                    .withExposedPorts(ELASTIC_PORT)
                    .withEnv("cluster.name", ELASTIC_CLUSTER_NAME)
                    .withEnv("discovery.type", "single-node")
                    .withEnv("ES_JAVA_OPTS", "-Xms512m -Xmx512m")
                    .withEnv("xpack.security.enabled", "false");

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        applyProperties(applicationContext);
    }

    private void applyProperties(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "elasticsearch.host:" + getHost(),
                "elasticsearch.port:" + getPort(),
                "elasticsearch.cluster-name:" + ELASTIC_CLUSTER_NAME
        ).applyTo(applicationContext);
    }

    static {
        ELASTICSEARCH.start();
    }

    private static String getHost() {
        return ELASTICSEARCH.getContainerIpAddress();
    }

    private static int getPort() {
        return ELASTICSEARCH.getMappedPort(ELASTIC_PORT);
    }

}
