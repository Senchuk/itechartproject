package com.itechart.userservice.integration;

import static com.itechart.userservice.user.document.enums.UserScopeDoc.*;
import static com.itechart.userservice.user.entity.enums.UserScopeEntity.ROLE_MAIN_STORAGE_MANAGER;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.itechart.userservice.generated.model.*;
import com.itechart.userservice.hospital.repository.HospitalRepository;
import com.itechart.userservice.integration.initializer.ElasticsearchInitializer;
import com.itechart.userservice.integration.initializer.PostgresInitializer;
import com.itechart.userservice.inventory.document.InventoryDocument;
import com.itechart.userservice.inventory.repository.InventoryRepository;
import com.itechart.userservice.inventory.repository.elastic.InventoryElasticRepository;
import com.itechart.userservice.medicine.document.MedicineDocument;
import com.itechart.userservice.medicine.document.enums.MedicineTypeDocument;
import com.itechart.userservice.medicine.entity.enums.MedicineTypeEntity;
import com.itechart.userservice.medicine.repository.MedicineRepository;
import com.itechart.userservice.medicine.repository.elastic.MedicineElasticRepository;
import com.itechart.userservice.order.repository.OrderRepository;
import com.itechart.userservice.order.repository.OrderRequestRepository;
import com.itechart.userservice.patient.repository.PatientRepository;
import com.itechart.userservice.patient.repository.elastic.PatientElasticRepository;
import com.itechart.userservice.research.repository.ResearchRepository;
import com.itechart.userservice.research.repository.elastic.ResearchElasticRepository;
import com.itechart.userservice.storage.repository.StorageRepository;
import com.itechart.userservice.user.repository.elastic.UserElasticRepository;
import com.itechart.userservice.visit.repository.VisitRepository;

import org.junit.Before;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {
        ElasticsearchInitializer.class,
        PostgresInitializer.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {

    protected static final String BASE_URL = "/api/v1";
    protected static final String AUTH_HEADER = "Bearer ";
    private static final String DELETE_USER_QUERY = "DELETE FROM \"user\" WHERE scope != :scope";
    private static final String DELETE_STORAGE_QUERY = "DELETE FROM Storage WHERE hospital_id IS NOT NULL";
    private static final String INSERT_MEDICINE =
            "INSERT INTO Medicine (id, name, producer, description, type, expiration_date, created_at) " +
                    "VALUES (:id, :name, :producer, :description, :type, :expirationDate, current_date)";
    private static final String INSERT_INVENTORY =
            "INSERT INTO Inventory (id, storage_id, medicine_id, quantity, order_id, created_at) " +
                    "VALUES (:id, :storageId, :medicineId, :quantity, :orderId, current_date)";
    private static final String SELECT_INVENTORY_DELETED_AT =
            "SELECT deleted_at FROM inventory WHERE medicine_id = :medicineId";
    private static final String SELECT_MEDICINE_DELETED_AT = "SELECT deleted_at FROM medicine WHERE id = :id";

    @Autowired
    private HospitalRepository hospitalRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private MedicineRepository medicineRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderRequestRepository orderRequestRepository;
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private ResearchRepository researchRepository;
    @Autowired
    private StorageRepository storageRepository;
    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private InventoryElasticRepository inventoryElasticRepository;
    @Autowired
    private MedicineElasticRepository medicineElasticRepository;
    @Autowired
    private PatientElasticRepository patientElasticRepository;
    @Autowired
    private ResearchElasticRepository researchElasticRepository;
    @Autowired
    private UserElasticRepository userElasticRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;

    protected TokenResponse signIn(SignInRequest user) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/auth/signin")
                .content(objectMapper.writeValueAsString(user))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), TokenResponse.class);
    }

    protected User singUpUser(SignUpRequest signUpRequest) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/auth/signup")
                .content(objectMapper.writeValueAsString(signUpRequest))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);
    }

    protected void blockUser(UUID id, String accessToken) throws Exception {
        mockMvc.perform(get(BASE_URL + "/users/" + id + "/block")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken))
                .andExpect(status().isOk())
                .andReturn();
    }

    protected void deleteUser(UUID id, String accessToken) throws Exception {
        mockMvc.perform(delete(BASE_URL + "/users/" + id)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    protected Patient createPatient(Patient patient, String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/patients")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(patient)))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Patient.class);
    }

    protected Medicine createMedicine(Medicine medicine, String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/medicines")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(medicine)))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Medicine.class);
    }

    protected Hospital createHospital(Hospital hospital, String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/hospitals")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(hospital)))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Hospital.class);
    }

    protected Research createResearch(Research research, String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/researches")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(research)))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Research.class);
    }

    protected Research createInventory(CreateInventoryRequest createInventoryRequest, String accessToken)
            throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/inventories")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(createInventoryRequest)))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Research.class);
    }

    protected Order createOrder(CreateOrder createOrder, String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/orders")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(createOrder)))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Order.class);
    }

    protected OrderRequest createOrderRequest(CreateOrderRequest createOrderRequest, String accessToken)
            throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/orders/requests")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(createOrderRequest)))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), OrderRequest.class);
    }

    protected Order updateMedicineStatus(
            UpdateMedicineStatusRequest updateMedicineStatusRequest, String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(put(BASE_URL + "/orders")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken)
                .content(objectMapper.writeValueAsString(updateMedicineStatusRequest)))
                .andExpect(status().isOk())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Order.class);
    }

    protected Visit createVisit(UUID patientId, String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/visits/" + patientId.toString())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, AUTH_HEADER + accessToken))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Visit.class);
    }

    protected void setMedicineIntoDatabase(UUID id, String name, String producer, String description,
                                                     MedicineTypeEntity type, Date expirationDate) {
        var params = new HashMap<String, Object>();
        params.put("id", id);
        params.put("name", name);
        params.put("producer", producer);
        params.put("description", description);
        params.put("type", type.toString());
        params.put("expirationDate", expirationDate);
       namedParameterJdbcTemplate.update(INSERT_MEDICINE, params);
    }

    protected MedicineDocument setMedicineIntoElastic(UUID id, String name, String producer, String description,
                                           MedicineTypeDocument type, Date expirationDate) {
        var medicine = new MedicineDocument();
        medicine.setId(id);
        medicine.setName(name);
        medicine.setProducer(producer);
        medicine.setDescription(description);
        medicine.setExpirationDate(expirationDate);
        medicine.setType(type);
        medicine.setCreatedAt(Date.from(Instant.now()));
        medicineElasticRepository.save(medicine);
        return medicine;
    }

    protected void setInventoryIntoDatabase(UUID id, UUID storageId, UUID medicineId, Integer quantity,
                                           UUID orderId) {
        var params = new HashMap<String, Object>();
        params.put("id", id);
        params.put("storageId", storageId);
        params.put("medicineId", medicineId);
        params.put("quantity", quantity);
        params.put("orderId", orderId);
        namedParameterJdbcTemplate.update(INSERT_INVENTORY, params);
    }

    protected void setInventoryIntoElastic(UUID id, UUID storageId, MedicineDocument medicine, Integer quantity,
                                           UUID orderId) {
        var inventory = new InventoryDocument();
        inventory.setId(id);
        inventory.setStorageId(storageId);
        inventory.setMedicine(medicine);
        inventory.setQuantity(quantity);
        inventory.setOrderId(orderId);
        inventoryElasticRepository.save(inventory);
    }

    protected Instant getMedicineDeletedAtById(UUID id) {
        var params = new HashMap<String, Object>();
        params.put("id", id);
        return namedParameterJdbcTemplate.queryForObject(SELECT_MEDICINE_DELETED_AT, params, Instant.class);
    }

    protected Instant getInventoryDeletedAtByMedicineId(UUID id) {
        var params = new HashMap<String, Object>();
        params.put("medicineId", id);
        return namedParameterJdbcTemplate.queryForObject(SELECT_INVENTORY_DELETED_AT, params, Instant.class);
    }

    private void deleteUsers() {
        var params = new HashMap<String, Object>();
        params.put("scope", ROLE_MAIN_STORAGE_MANAGER.toString());
        namedParameterJdbcTemplate.update(DELETE_USER_QUERY, params);
    }

    @Before
    public void clearDatabase() {
        visitRepository.deleteAll();
        orderRequestRepository.deleteAll();
        inventoryRepository.deleteAll();
        orderRepository.deleteAll();
        researchRepository.deleteAll();
        medicineRepository.deleteAll();
        jdbcTemplate.update(DELETE_STORAGE_QUERY);
        patientRepository.deleteAll();
        storageRepository.deleteAll();
        hospitalRepository.deleteAll();
        deleteUsers();
        
        inventoryElasticRepository.deleteAll();
        researchElasticRepository.deleteAll();
        medicineElasticRepository.deleteAll();
        patientElasticRepository.deleteAll();
        userElasticRepository.deleteByScope(ROLE_SPONSOR);
        userElasticRepository.deleteByScope(ROLE_RESEARCHER);
        userElasticRepository.deleteByScope(ROLE_STORAGE_MANAGER);
    }

}
