package com.itechart.userservice.integration.researchesApiTest;

import static com.itechart.userservice.generated.model.UserScope.*;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.Research;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class ResearchesControllerTest extends AbstractIntegrationTest {

    private static final String RESEARCH_URL = BASE_URL + "/researches";

    //region create research tests

    @Test
    public void createResearch_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                            "Password123", RESEARCHER);
        var savedResearcher = singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(),
                researcherSignUpRequest.getPassword());

        var researcherTokenResponse = signIn(researcherSignInRequest);
        var research = research("test", savedHospital.getId());

        //WHEN
        var mvcResult =
                mockMvc.perform(post(RESEARCH_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(research)))
                        .andExpect(status().isCreated())
                        .andReturn();

        final Research response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Research>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getName()).isEqualTo(research.getName());
            softly.assertThat(response.getResearcherId()).isEqualTo(savedResearcher.getId());
            softly.assertThat(response.getHospitalId()).isEqualTo(savedHospital.getId());
        });
    }

    @Test
    public void createResearchWhenResearcherIsBusy() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var aponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(aponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var firstResearch = research("test", savedHospital.getId());
        createResearch(firstResearch, researcherTokenResponse.getAccessToken());

        var secondResearch = research("test", savedHospital.getId());

        //WHEN
        var resultActions =
                mockMvc.perform(post(RESEARCH_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(secondResearch)));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    //endregion

    //region get research by researcher id tests

    @Test
    public void getResearchByResearcherId_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var aponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(aponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        var savedResearcher = singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var research = research("test", savedHospital.getId());
        createResearch(research, researcherTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(get(RESEARCH_URL + "/researcher/" + savedResearcher.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final Research response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Research>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getName()).isEqualTo(research.getName());
            softly.assertThat(response.getResearcherId()).isEqualTo(savedResearcher.getId());
            softly.assertThat(response.getHospitalId()).isEqualTo(savedHospital.getId());
        });
    }

    @Test
    public void getResearchByResearcherIdWhenResearchNotFound() throws Exception {
        //GIVEN
        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        var savedResearcher = singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        //WHEN
        var resultActions =
                mockMvc.perform(get(RESEARCH_URL + "/researcher/" + savedResearcher.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void getResearchByResearcherIdWhenUserIsNotResearcher() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        var savedSponsor = singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        //WHEN
        var resultActions =
                mockMvc.perform(get(RESEARCH_URL + "/researcher/" + savedSponsor.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + sponsorTokenResponse.getAccessToken()));

        //THEN
        resultActions.andExpect(status().isForbidden());
    }

    //endregion

    //region get all research tests

    @Test
    public void getAllResearch_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var research = research("test", savedHospital.getId());
        createResearch(research, researcherTokenResponse.getAccessToken());

        var sort = new String[]{"+name"};
        var search = "test";

        //WHEN
        var mvcResult =
                mockMvc.perform(get(RESEARCH_URL + "/all")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("search", search))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Research> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Research>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(1);
    }

    @Test
    public void getEmptyResearchPage_happyPath() throws Exception {
        //GIVEN
        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest(researcherSignUpRequest.getEmail(), researcherSignUpRequest.getPassword());
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var research = research("test", savedHospital.getId());
        createResearch(research, researcherTokenResponse.getAccessToken());

        var sort = new String[]{"+name"};
        var search = "test";

        //WHEN
        var mvcResult =
                mockMvc.perform(get(RESEARCH_URL + "/all")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken())
                        .param("page", "2")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("search", search))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Research> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Research>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(0);

        //endregion
    }

}
