package com.itechart.userservice.integration.medicinesApiTest;

import static com.itechart.userservice.generated.model.MedicineStatus.WHOLE;
import static com.itechart.userservice.generated.model.MedicineType.CAPSULE;
import static com.itechart.userservice.generated.model.MedicineType.DROPS;
import static com.itechart.userservice.generated.model.OrderRequestStatus.ACCEPTED;
import static com.itechart.userservice.generated.model.UserScope.*;
import static com.itechart.userservice.utils.TestUtils.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.generated.model.Medicine;
import com.itechart.userservice.integration.AbstractIntegrationTest;

import org.junit.Test;

public class MedicinesControllerTest extends AbstractIntegrationTest {

    private static final String MEDICINES_URL = BASE_URL + "/medicines";

    //region create medicine tests

    @Test
    public void createMedicine_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
               CAPSULE, Date.from(Instant.now().plusSeconds(99999)));

        //WHEN
        var mvcResult =
                mockMvc.perform(post(MEDICINES_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken())
                        .content(objectMapper.writeValueAsString(medicine)))
                        .andExpect(status().isCreated())
                        .andReturn();

        final Medicine response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Medicine>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getId()).isNotNull();
            softly.assertThat(response.getName()).isEqualTo(medicine.getName());
            softly.assertThat(response.getProducer()).isEqualTo(medicine.getProducer());
            softly.assertThat(response.getDescription()).isEqualTo(medicine.getDescription());
            softly.assertThat(response.getType()).isEqualTo(medicine.getType());
        });
    }

    //endregion

    //region get all medicines tests

    @Test
    public void getAllMedicines_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var firstMedicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(firstMedicine, mainManagerTokenResponse.getAccessToken());

        var secondMedicine = medicine("test", "test", "test description",
                DROPS, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(secondMedicine, mainManagerTokenResponse.getAccessToken());

        var sort = new String[]{"+name"};
        var filter = new String[]{"DROPS"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(MEDICINES_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("filter", filter)
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Medicine> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Medicine>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(1);
    }

    @Test
    public void getEmptyMedicinePage() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var firstMedicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(firstMedicine, mainManagerTokenResponse.getAccessToken());

        var secondMedicine = medicine("test", "test", "test description",
                DROPS, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(secondMedicine, mainManagerTokenResponse.getAccessToken());

        var sort = new String[]{"+name"};
        var filter = new String[]{"DROPS"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(MEDICINES_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken())
                        .param("page", "2")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("filter", filter)
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();

        final List<Medicine> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Medicine>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(0);
    }

    @Test
    public void getMedicinePageWithoutFilter() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var firstMedicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(firstMedicine, mainManagerTokenResponse.getAccessToken());

        var secondMedicine = medicine("test", "test", "test description",
                DROPS, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(secondMedicine, mainManagerTokenResponse.getAccessToken());

        var sort = new String[]{"+name"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(MEDICINES_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("search", "test"))
                        .andExpect(status().isOk())
                        .andReturn();


        final List<Medicine> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Medicine>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(2);
    }

    @Test
    public void getMedicinePageWithoutSearch() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var firstMedicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(firstMedicine, mainManagerTokenResponse.getAccessToken());

        var secondMedicine = medicine("test", "test", "test description",
                DROPS, Date.from(Instant.now().plusSeconds(99999)));
        createMedicine(secondMedicine, mainManagerTokenResponse.getAccessToken());

        var sort = new String[]{"+name"};
        var filter = new String[]{"DROPS"};

        //WHEN
        var mvcResult =
                mockMvc.perform(get(MEDICINES_URL)
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken())
                        .param("page", "1")
                        .param("size", "2")
                        .param("sort", sort)
                        .param("filter", filter))
                        .andExpect(status().isOk())
                        .andReturn();


        final List<Medicine> response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Medicine>>() {
                });

        //THEN
        assertThat(response.size()).isEqualTo(1);
    }

    //endregion

    //region get by id medicine tests

    @Test
    public void getMedicineById_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(get(MEDICINES_URL + "/" + savedMedicine.getId())
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + mainManagerTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final Medicine response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Medicine>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response).isNotNull();
            softly.assertThat(response.getId()).isEqualTo(savedMedicine.getId());
            softly.assertThat(response.getName()).isEqualTo(savedMedicine.getName());
            softly.assertThat(response.getProducer()).isEqualTo(savedMedicine.getProducer());
            softly.assertThat(response.getDescription()).isEqualTo(savedMedicine.getDescription());
            softly.assertThat(response.getType()).isEqualTo(savedMedicine.getType());
        });
    }

    //endregion

    //region get random medicine tests

    @Test
    public void getRandomMedicine_happyPath() throws Exception {
        //GIVEN
        var mainManagerSignInRequest = createSignInRequest("main@mail.com", "Password123");
        var mainManagerTokenResponse = signIn(mainManagerSignInRequest);

        var medicine = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now().plusSeconds(99999)));
        var savedMedicine = createMedicine(medicine, mainManagerTokenResponse.getAccessToken());

        var inventory = inventory(savedMedicine.getId(), 10);
        createInventory(inventory, mainManagerTokenResponse.getAccessToken());

        var sponsorSignUpRequest = createSignUpRequest("test", "test", "sponsor@mail.com",
                "Password123", SPONSOR);
        singUpUser(sponsorSignUpRequest);
        var sponsorSignInRequest = createSignInRequest(sponsorSignUpRequest.getEmail(), sponsorSignUpRequest.getPassword());
        var sponsorTokenResponse = signIn(sponsorSignInRequest);

        var managerSignUpRequest = createSignUpRequest("test", "test", "manager@mail.com",
                "Password123", STORAGE_MANAGER);
        var savedManager = singUpUser(managerSignUpRequest);

        var hospital = hospital("test", "test", savedManager.getId());
        var savedHospital = createHospital(hospital, sponsorTokenResponse.getAccessToken());

        var managerSignInRequest = createSignInRequest(managerSignUpRequest.getEmail(), managerSignUpRequest.getPassword());
        var managerTokenResponse = signIn(managerSignInRequest);

        var order = order(savedMedicine.getId(), 5);
        var savedOrder = createOrder(order, managerTokenResponse.getAccessToken());

        var mainManagerSecondTokenResponse = signIn(mainManagerSignInRequest);

        var orderRequest = orderRequest(savedOrder.getId(), ACCEPTED);
        createOrderRequest(orderRequest, mainManagerSecondTokenResponse.getAccessToken());

        var managerSecondTokenResponse = signIn(managerSignInRequest);

        var updateMedicineStatus = updateMedicineStatusRequest(savedOrder.getId(), WHOLE);
        updateMedicineStatus(updateMedicineStatus, managerSecondTokenResponse.getAccessToken());

        var researcherSignUpRequest = createSignUpRequest("test", "test", "researcher@mail.com",
                "Password123", RESEARCHER);
        singUpUser(researcherSignUpRequest);
        var researcherSignInRequest = createSignInRequest("researcher@mail.com", "Password123");
        var researcherTokenResponse = signIn(researcherSignInRequest);

        var research = research("test", savedHospital.getId());
        createResearch(research, researcherTokenResponse.getAccessToken());

        //WHEN
        var mvcResult =
                mockMvc.perform(get(MEDICINES_URL + "/random")
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, AUTH_HEADER + researcherTokenResponse.getAccessToken()))
                        .andExpect(status().isOk())
                        .andReturn();

        final Medicine response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), new TypeReference<Medicine>() {
                });

        //THEN
        assertThat(response).isNotNull();
    }

    //endregion

}
