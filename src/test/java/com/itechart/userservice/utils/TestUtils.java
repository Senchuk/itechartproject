package com.itechart.userservice.utils;

import java.util.Date;
import java.util.UUID;

import com.itechart.userservice.generated.model.*;

public class TestUtils {

    public static SignUpRequest createSignUpRequest(String firstName, String lastName, String email,
                                                    String password, UserScope userScope) {
        var signUpRequest = new SignUpRequest();
        signUpRequest.setFirstName(firstName);
        signUpRequest.setLastName(lastName);
        signUpRequest.setEmail(email);
        signUpRequest.setPassword(password);
        signUpRequest.setScope(userScope);
        return signUpRequest;
    }

    public static User createUser(UUID id, String firstName, String lastName, String email,
                                  UserScope userScope) {
        var user = new User();
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setScope(userScope);
        return user;
    }

    public static SignInRequest createSignInRequest(String email, String password) {
        var signInRequest = new SignInRequest();
        signInRequest.setEmail(email);
        signInRequest.setPassword(password);
        return signInRequest;
    }

    public static ChangePasswordRequest createChangePasswordRequest(String oldPassword, String newPassword) {
        var changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setOldPassword(oldPassword);
        changePasswordRequest.setNewPassword(newPassword);
        return changePasswordRequest;
    }

    public static Patient patient(String firstName, String lastName, String secondName,
                                  PatientGender gender, Date dob, PatientStatus status) {
        var patient = new Patient();
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setSecondName(secondName);
        patient.setGender(gender);
        patient.setDob(dob);
        patient.setStatus(status);
        return patient;
    }

    public static ChangeStatusRequest createChangeStatusRequest(PatientStatus status) {
        var updateStatusRequest = new ChangeStatusRequest();
        updateStatusRequest.setStatus(status);
        return updateStatusRequest;
    }

    public static Medicine medicine(String name, String producer, String description,
                                    MedicineType type, Date expirationDate) {
        var medicine = new Medicine();
        medicine.setName(name);
        medicine.setProducer(producer);
        medicine.setDescription(description);
        medicine.setType(type);
        medicine.setExpirationDate(expirationDate);
        return medicine;
    }

    public static Hospital hospital(String name, String address, UUID userId) {
        var hospital = new Hospital();
        hospital.setName(name);
        hospital.setAddress(address);
        hospital.setManagerId(userId);
        return hospital;
    }

    public static Research research(String name, UUID hospitalId) {
        var research = new Research();
        research.setName(name);
        research.setHospitalId(hospitalId);
        return research;
    }

    public static CreateInventoryRequest inventory(UUID medicineId, Integer quantity) {
        var createInventoryRequest = new CreateInventoryRequest();
        createInventoryRequest.setMedicineId(medicineId);
        createInventoryRequest.setQuantity(quantity);
        return createInventoryRequest;
    }

    public static CreateOrder order(UUID medicineId, Integer quantity) {
        var orderRequest = new CreateOrder();
        orderRequest.setMedicineId(medicineId);
        orderRequest.setQuantity(quantity);
        return orderRequest;
    }

    public static CreateOrderRequest orderRequest(UUID orderId, OrderRequestStatus orderRequestStatus) {
        var orderRequest = new CreateOrderRequest();
        orderRequest.setOrderId(orderId);
        orderRequest.setOrderRequestStatus(orderRequestStatus);
        return orderRequest;
    }

    public static UpdateMedicineStatusRequest updateMedicineStatusRequest(UUID orderId, MedicineStatus medicineStatus) {
        var updateOrderStatusRequest = new UpdateMedicineStatusRequest();
        updateOrderStatusRequest.setId(orderId);
        updateOrderStatusRequest.setMedicineStatus(medicineStatus);
        return updateOrderStatusRequest;
    }

}
