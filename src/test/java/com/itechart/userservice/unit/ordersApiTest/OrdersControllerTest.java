package com.itechart.userservice.unit.ordersApiTest;

import static com.itechart.userservice.generated.model.MedicineStatus.WHOLE;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.*;
import static com.itechart.userservice.utils.TestUtils.order;
import static com.itechart.userservice.utils.TestUtils.updateMedicineStatusRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.Order;
import com.itechart.userservice.order.service.OrderService;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class OrdersControllerTest extends AbstractUnitTest {

    private static final String ORDERS_URL = BASE_URL + "/orders";

    @Mock
    private OrderService orderService;

    //region create order tests

    @Test
    public void createOrder_happyPath() throws Exception {
        //GIVEN
        var request = order(UUID.randomUUID(), 5);
        given(orderService.create(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(post(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void createOrderWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = order(null, null);
        given(orderService.create(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(post(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(2);
        });
    }

    @Test
    public void createOrderWithoutQuantity() throws Exception {
        //GIVEN
        var request = order(UUID.randomUUID(), null);
        given(orderService.create(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(post(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createOrderWhenMinQuantityIsInvalid() throws Exception {
        //GIVEN
        var request = order(UUID.randomUUID(), 0);
        given(orderService.create(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(post(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(QUANTITY_MIN_IS_INVALID.getMessage());
        });
    }

    @Test
    public void createOrderWhenMaxQuantityIsInvalid() throws Exception {
        //GIVEN
        var request = order(UUID.randomUUID(), 11);
        given(orderService.create(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(post(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(QUANTITY_MAX_IS_INVALID.getMessage());
        });
    }

    @Test
    public void createOrderWithoutMedicineId() throws Exception {
        //GIVEN
        var request = order(null, 5);
        given(orderService.create(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(post(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

    //region update medicine status tests

    @Test
    public void updateMedicineStatus_happyPath() throws Exception {
        //GIVEN
        var request = updateMedicineStatusRequest(UUID.randomUUID(), WHOLE);
        given(orderService.updateStatus(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(put(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void updateMedicineStatusWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = updateMedicineStatusRequest(null, null);
        given(orderService.updateStatus(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(put(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(2);
        });
    }

    @Test
    public void updateMedicineStatusWithoutOrderId() throws Exception {
        //GIVEN
        var request = updateMedicineStatusRequest(null,  WHOLE);
        given(orderService.updateStatus(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(put(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void updateMedicineStatusWithoutMedicineStatus() throws Exception {
        //GIVEN
        var request = updateMedicineStatusRequest(UUID.randomUUID(), null);
        given(orderService.updateStatus(request)).willReturn(new Order());

        //WHEN
        var response = mockMvc.perform(put(ORDERS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

}
