package com.itechart.userservice.unit.authApiTest;

import static com.itechart.userservice.generated.model.UserScope.SPONSOR;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.*;
import static com.itechart.userservice.utils.TestUtils.createSignInRequest;
import static com.itechart.userservice.utils.TestUtils.createSignUpRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.authentication.service.AuthService;
import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.TokenResponse;
import com.itechart.userservice.generated.model.User;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class AuthControllerTest extends AbstractUnitTest {

    private static final String AUTH_URL = BASE_URL + "/auth";

    @Mock
    private AuthService authenticationService;

    //region signUp tests

    @Test
    public void signUpUser_happyPath() throws Exception {
        //GIVEN
        var request = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void signUpUserWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = createSignUpRequest("", null, "test",
                "password", null);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(5);
        });
    }

    @Test
    public void signUpUserWithoutFirstName() throws Exception {
        //GIVEN
        var request = createSignUpRequest(null, "test", "test@mail.com",
                "Password123", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void signUpUserWhenFirstNameIsInvalid() throws Exception {
        //GIVEN
        var request = createSignUpRequest("test12&%?", "test", "test@mail.com",
                "Password123", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(NAME_IS_INVALID.getMessage());
        });
    }

    @Test
    public void signUpUserWithoutLastName() throws Exception {
        //GIVEN
        var request = createSignUpRequest("test", null, "test@mail.com",
                "Password123", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void signUpUserWhenLastNameIsInvalid() throws Exception {
        //GIVEN
        var request = createSignUpRequest("test", "test12&%?", "test@mail.com",
                "Password123", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(NAME_IS_INVALID.getMessage());
        });
    }

    @Test
    public void signUpUserWithoutEmail() throws Exception {
        //GIVEN
        var request = createSignUpRequest("test", "test", null,
                "Password123", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void signUpUserWhenEmailIsInvalid() throws Exception {
        //GIVEN
        var request = createSignUpRequest( "test", "test", "test",
                "Password123", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
            .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(EMAIL_IS_INVALID.getMessage());
        });
    }

    @Test
    public void signUpUserWithoutPassword() throws Exception {
        //GIVEN
        var request = createSignUpRequest("test", "test", "test@mail.com",
                null, SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void signUpUserWhenPasswordIsInvalid() throws Exception {
        //GIVEN
        var request = createSignUpRequest( "test", "test", "test@mail.com",
                "password", SPONSOR);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(PASSWORD_IS_INVALID.getMessage());
        });
    }


    @Test
    public void signUpUserWithoutScope() throws Exception {
        //GIVEN
        var request = createSignUpRequest("test", "test", "test@mail.com",
                "Password123", null);
        given(authenticationService.signUp(request)).willReturn(new User());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

    //region signIn tests

    @Test
    public void signInUser_happyPath() throws Exception {
        //GIVEN
        var request = createSignInRequest("test@mail.com", "Password123");
        given(authenticationService.signIn(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void signInUserWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = createSignInRequest("test", null);
        given(authenticationService.signIn(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(2);
        });
    }

    @Test
    public void signInUserWithoutEmail() throws Exception {
        //GIVEN
        var request = createSignInRequest(null, "Password123");
        given(authenticationService.signIn(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void signInUserWhenEmailIsInvalid() throws Exception {
        //GIVEN
        var request = createSignInRequest("test", "Password123");
        given(authenticationService.signIn(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(EMAIL_IS_INVALID.getMessage());
        });
    }

    @Test
    public void signInUserWithoutPassword() throws Exception {
        //GIVEN
        var request = createSignInRequest("test@mail.com", null);
        given(authenticationService.signIn(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

}
