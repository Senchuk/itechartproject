package com.itechart.userservice.unit.orderRequestsApiTest;

import static com.itechart.userservice.generated.model.OrderRequestStatus.ACCEPTED;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.VALUE_IS_NULL;
import static com.itechart.userservice.utils.TestUtils.orderRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.OrderRequest;
import com.itechart.userservice.order.service.OrderRequestService;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class OrderRequestsControllerTest extends AbstractUnitTest {

    private static final String ORDER_REQUESTS_URL = BASE_URL + "/orders/requests";

    @Mock
    private OrderRequestService orderRequestService;

    //region create order request tests

    @Test
    public void createOrderRequest_happyPath() throws Exception {
        //GIVEN
        var request = orderRequest(UUID.randomUUID(), ACCEPTED);
        given(orderRequestService.create(request)).willReturn(new OrderRequest());

        //WHEN
        var response = mockMvc.perform(post(ORDER_REQUESTS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void createOrderRequestWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = orderRequest(null, null);
        given(orderRequestService.create(request)).willReturn(new OrderRequest());

        //WHEN
        var response = mockMvc.perform(post(ORDER_REQUESTS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(2);
        });
    }

    @Test
    public void createOrderRequestWithoutOrderId() throws Exception {
        //GIVEN
        var request = orderRequest(null, ACCEPTED);
        given(orderRequestService.create(request)).willReturn(new OrderRequest());

        //WHEN
        var response = mockMvc.perform(post(ORDER_REQUESTS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createOrderRequestWithoutStatus() throws Exception {
        //GIVEN
        var request = orderRequest(UUID.randomUUID(), null);
        given(orderRequestService.create(request)).willReturn(new OrderRequest());

        //WHEN
        var response = mockMvc.perform(post(ORDER_REQUESTS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

}
