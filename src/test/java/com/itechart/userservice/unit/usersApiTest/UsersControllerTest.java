package com.itechart.userservice.unit.usersApiTest;

import static com.itechart.userservice.generated.model.UserScope.SPONSOR;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.*;
import static com.itechart.userservice.utils.TestUtils.createChangePasswordRequest;
import static com.itechart.userservice.utils.TestUtils.createUser;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.unit.AbstractUnitTest;
import com.itechart.userservice.user.service.UserService;

import org.junit.Test;

import org.mockito.Mock;

public class UsersControllerTest extends AbstractUnitTest {

    private static final String USER_URL = BASE_URL +  "/users";

    @Mock
    private UserService userService;

    //region update user personal data tests

    @Test
    public void updateUser_happyPath() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), "test", "test",
                "updated@mail.com", SPONSOR);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void updateUserWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), null, "test12&%",
                "updated", null);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
        });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(4);
        });
    }

    @Test
    public void updateUserWithoutFirstName() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), null, "test",
                "updated@mail.com", SPONSOR);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void updateUserWhenFirstNameIsInvalid() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), "test12&%", "test",
                "updated@mail.com", SPONSOR);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(NAME_IS_INVALID.getMessage());
        });
    }

    @Test
    public void updateUserWithoutLastName() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), "test", null,
                "updated@mail.com", SPONSOR);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void updateUserWhenWhenLastNameIsInvalid() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), "test", "test12&%",
                "updated@mail.com", SPONSOR);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(NAME_IS_INVALID.getMessage());
        });
    }

    @Test
    public void updateUserWithoutEmail() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), "test", "test",
                null, SPONSOR);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void updateUserWhenEmailIsInvalid() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), "test", "test",
                "updated", SPONSOR);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(EMAIL_IS_INVALID.getMessage());
        });
    }

    @Test
    public void updateUserWithoutScope() throws Exception {
        //GIVEN
        var request = createUser(UUID.randomUUID(), "test", "test",
                "updated@mail.com", null);
        given(userService.update(request)).willReturn(request);

        //WHEN
        var response = mockMvc.perform(put(USER_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

    //region change user password tests

    @Test
    public void changePasswordUser_happyPath() throws Exception {
        //GIVEN
        var request = createChangePasswordRequest("Password123", "NewPassword123");

        //WHEN
        var response = mockMvc.perform(put(USER_URL + "/changePassword")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void changePasswordUserWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = createChangePasswordRequest(null, "password");

        //WHEN
        var response = mockMvc.perform(put(USER_URL + "/changePassword")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(2);
        });
    }

    @Test
    public void changePasswordUserWithoutOldPassword() throws Exception {
        //GIVEN
        var request = createChangePasswordRequest(null, "Password123");

        //WHEN
        var response = mockMvc.perform(put(USER_URL + "/changePassword")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });

    }

    @Test
    public void changePasswordUserWithoutNewPassword() throws Exception {
        //GIVEN
        var request = createChangePasswordRequest("Password123", null);

        //WHEN
        var response = mockMvc.perform(put(USER_URL + "/changePassword")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void changePasswordUserWhenNewPasswordIsInvalid() throws Exception {
        //GIVEN
        var request = createChangePasswordRequest("Password123", "password");

        //WHEN
        var response = mockMvc.perform(put(USER_URL + "/changePassword")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(PASSWORD_IS_INVALID.getMessage());
        });
    }

    //endregion

}
