package com.itechart.userservice.unit;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.itechart.userservice.UserServiceBackendApplication;
import com.itechart.userservice.authentication.api.AuthApiImpl;
import com.itechart.userservice.exception.ExceptionHandling;
import com.itechart.userservice.hospital.api.HospitalsApiImpl;
import com.itechart.userservice.inventory.api.InventoriesApiImpl;
import com.itechart.userservice.medicine.api.MedicinesApiImpl;
import com.itechart.userservice.order.api.OrdersApiImpl;
import com.itechart.userservice.patient.api.PatientsApiImpl;
import com.itechart.userservice.research.api.ResearchesApiImpl;
import com.itechart.userservice.user.api.UsersApiImpl;

import org.junit.Before;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = UserServiceBackendApplication.class)
public abstract class AbstractUnitTest {

    protected static final String BASE_URL = "/api/v1";

    @InjectMocks
    private AuthApiImpl authApiImpl;
    @InjectMocks
    private HospitalsApiImpl hospitalsApiImpl;
    @InjectMocks
    private InventoriesApiImpl inventoriesApiImpl;
    @InjectMocks
    private MedicinesApiImpl medicinesApiImpl;
    @InjectMocks
    private OrdersApiImpl ordersApiImpl;
    @InjectMocks
    private PatientsApiImpl patientsApiImpl;
    @InjectMocks
    private ResearchesApiImpl researchesApiImpl;
    @InjectMocks
    private UsersApiImpl usersApiImpl;

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.objectMapper = new ObjectMapper();
        this.mockMvc = MockMvcBuilders.standaloneSetup(
                authApiImpl, usersApiImpl, patientsApiImpl, inventoriesApiImpl, ordersApiImpl,
                medicinesApiImpl, hospitalsApiImpl, researchesApiImpl)
                .setControllerAdvice(new ExceptionHandling())
                .build();
    }

}
