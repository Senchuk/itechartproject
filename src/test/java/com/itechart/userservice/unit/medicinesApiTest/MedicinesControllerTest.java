package com.itechart.userservice.unit.medicinesApiTest;

import static com.itechart.userservice.generated.model.MedicineType.CAPSULE;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.VALUE_IS_NULL;
import static com.itechart.userservice.utils.TestUtils.medicine;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.Medicine;
import com.itechart.userservice.medicine.service.MedicineService;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class MedicinesControllerTest extends AbstractUnitTest {

    private static final String MEDICINES_URL = BASE_URL + "/medicines";

    @Mock
    private MedicineService medicineService;

    //region create medicines tests

    @Test
    public void createMedicine_happyPath() throws Exception {
        //GIVEN
        var request = medicine("test", "test", "test description",
                CAPSULE, Date.from(Instant.now()));
        given(medicineService.create(request)).willReturn(new Medicine());

        //WHEN
        var response = mockMvc.perform(post(MEDICINES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void createMedicineWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = medicine(null, null, null,
                null, null);
        given(medicineService.create(request)).willReturn(new Medicine());

        //WHEN
        var response = mockMvc.perform(post(MEDICINES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(5);
        });
    }

    @Test
    public void createMedicineWithoutName() throws Exception {
        //GIVEN
        var request = medicine(null, "test", "test description",
                CAPSULE, Date.from(Instant.now()));
        given(medicineService.create(request)).willReturn(new Medicine());

        //WHEN
        var response = mockMvc.perform(post(MEDICINES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createMedicineWithoutProducer() throws Exception {
        //GIVEN
        var request = medicine("test", null, "test description",
                CAPSULE, Date.from(Instant.now()));
        given(medicineService.create(request)).willReturn(new Medicine());

        //WHEN
        var response = mockMvc.perform(post(MEDICINES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createMedicineWithoutDescription() throws Exception {
        //GIVEN
        var request = medicine("test", "test", null,
                CAPSULE, Date.from(Instant.now()));
        given(medicineService.create(request)).willReturn(new Medicine());

        //WHEN
        var response = mockMvc.perform(post(MEDICINES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createMedicineWithoutType() throws Exception {
        //GIVEN
        var request = medicine("test", "test", "test description",
                null, Date.from(Instant.now()));
        given(medicineService.create(request)).willReturn(new Medicine());

        //WHEN
        var response = mockMvc.perform(post(MEDICINES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createMedicineWithoutExpirationDate() throws Exception {
        //GIVEN
        var request = medicine("test", "test", "test description",
                CAPSULE, null);
        given(medicineService.create(request)).willReturn(new Medicine());

        //WHEN
        var response = mockMvc.perform(post(MEDICINES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

}
