package com.itechart.userservice.unit.patientsApiTest;

import static com.itechart.userservice.generated.model.PatientGender.MALE;
import static com.itechart.userservice.generated.model.PatientStatus.ACCEPTED_INTO_THE_RESEARCHING;
import static com.itechart.userservice.generated.model.PatientStatus.SCREENED;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.NAME_IS_INVALID;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.VALUE_IS_NULL;
import static com.itechart.userservice.utils.TestUtils.createChangeStatusRequest;
import static com.itechart.userservice.utils.TestUtils.patient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.Patient;
import com.itechart.userservice.patient.service.PatientService;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class PatientsControllerTest extends AbstractUnitTest {

    private static final String PATIENT_URL = BASE_URL + "/patients";

    @Mock
    private PatientService patientService;

    //region create patient tests

    @Test
    public void createPatient_happyPath() throws Exception {
        //GIVEN
        var response = patient("test", "test", "test",
                MALE, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(request.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void createPatientWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var response = patient(null, "", "test123",
                null, null, null);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(request.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(6);
        });
    }

    @Test
    public void createPatientWithoutFirstName() throws Exception {
        //GIVEN
        var response = patient(null, "test", "test",
                MALE, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createPatientWithInvalidFirstName() throws Exception {
        //GIVEN
        var response = patient("test123", "test", "test",
                MALE, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(NAME_IS_INVALID.getMessage());
        });
    }

    @Test
    public void createPatientWithoutLastName() throws Exception {
        //GIVEN
        var response = patient("test", null, "test",
                MALE, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createPatientWithInvalidLastName() throws Exception {
        //GIVEN
        var response = patient("test", "test123", "test",
                MALE, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(NAME_IS_INVALID.getMessage());
        });
    }

    @Test
    public void createPatientWithoutSecondName() throws Exception {
        //GIVEN
        var response = patient("test", "test", null,
                MALE, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createPatientWithInvalidSecondName() throws Exception {
        //GIVEN
        var response = patient("test", "test", "test123",
                MALE, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(NAME_IS_INVALID.getMessage());
        });
    }

    public void createPatientWithoutGender() throws Exception {
        //GIVEN
        var response = patient("test", "test", "test",
                null, Date.from(Instant.now()), SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    public void createPatientWithoutDateOfBirth() throws Exception {
        //GIVEN
        var response = patient("test", "test", "test",
                MALE, null, SCREENED);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    public void createPatientWithoutStatus() throws Exception {
        //GIVEN
        var response = patient("test", "test", "test",
                MALE, Date.from(Instant.now()), null);
        given(patientService.create(response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(post(PATIENT_URL)
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

    //region update patient status tests

    @Test
    public void updatePatientStatus_happyPath() throws Exception {
        //GIVEN
        var response = createChangeStatusRequest(ACCEPTED_INTO_THE_RESEARCHING);
        var id = UUID.randomUUID();
        given(patientService.updateStatus(id, response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(put(PATIENT_URL + "/" + id + "/status")
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(request.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    public void updatePatientStatusWithoutStatus() throws Exception {
        //GIVEN
        var response = createChangeStatusRequest(null);
        var id = UUID.randomUUID();
        given(patientService.updateStatus(id, response)).willReturn(new Patient());

        //WHEN
        var request = mockMvc.perform(put(PATIENT_URL + "/" + id + "/status")
                .content(objectMapper.writeValueAsString(response))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(request.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

}
