package com.itechart.userservice.unit.researchesApiTest;

import static com.itechart.userservice.unit.validationMessages.ValidationMessages.VALUE_IS_NULL;
import static com.itechart.userservice.utils.TestUtils.research;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.Research;
import com.itechart.userservice.research.service.ResearchService;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class ResearchesControllerTest extends AbstractUnitTest {

    private static final String RESEARCH_URL = BASE_URL + "/researches";

    @Mock
    private ResearchService researchService;

    //region create research tests

    @Test
    public void createResearch_happyPath() throws Exception {
        //GIVEN
        var request = research("test", UUID.randomUUID());
        given(researchService.create(request)).willReturn(new Research());

        //WHEN
        var response = mockMvc.perform(post(RESEARCH_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void createResearchWithMultipleInvalidFields() throws Exception {
        var request = research(null, null);
        given(researchService.create(request)).willReturn(new Research());

        //WHEN
        var response = mockMvc.perform(post(RESEARCH_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(2);
        });
    }

    @Test
    public void createResearchWithoutName() throws Exception {
        //GIVEN
        var request = research(null, UUID.randomUUID());
        given(researchService.create(request)).willReturn(new Research());

        //WHEN
        var response = mockMvc.perform(post(RESEARCH_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createResearchWithoutHospitalId() throws Exception {
        //GIVEN
        var request = research("test", null);
        given(researchService.create(request)).willReturn(new Research());

        //WHEN
        var response = mockMvc.perform(post(RESEARCH_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

}
