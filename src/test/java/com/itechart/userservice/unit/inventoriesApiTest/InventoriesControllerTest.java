package com.itechart.userservice.unit.inventoriesApiTest;

import static com.itechart.userservice.unit.validationMessages.ValidationMessages.QUANTITY_MIN_IS_INVALID;
import static com.itechart.userservice.unit.validationMessages.ValidationMessages.VALUE_IS_NULL;
import static com.itechart.userservice.utils.TestUtils.inventory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.CreateInventoryRequest;
import com.itechart.userservice.inventory.service.InventoryService;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class InventoriesControllerTest extends AbstractUnitTest {

    private static final String INVENTORIES_URL = BASE_URL + "/inventories";

    @Mock
    private InventoryService inventoryService;

    //region create main storage inventory tests

    @Test
    public void createMainStorageInventory_happyPath() throws Exception {
        //GIVEN
        var request = inventory(UUID.randomUUID(), 5);
        given(inventoryService.create(request)).willReturn(new CreateInventoryRequest());

        //WHEN
        var response = mockMvc.perform(post(INVENTORIES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void createMainStorageInventoryWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = inventory(null, null);
        given(inventoryService.create(request)).willReturn(new CreateInventoryRequest());

        //WHEN
        var response = mockMvc.perform(post(INVENTORIES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(2);
        });
    }

    @Test
    public void createMainStorageInventoryWithoutMedicineId() throws Exception {
        //GIVEN
        var request = inventory(null, 5);
        given(inventoryService.create(request)).willReturn(new CreateInventoryRequest());

        //WHEN
        var response = mockMvc.perform(post(INVENTORIES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createMainStorageInventoryWithoutQuantity() throws Exception {
        //GIVEN
        var request = inventory(UUID.randomUUID(), null);
        given(inventoryService.create(request)).willReturn(new CreateInventoryRequest());

        //WHEN
        var response = mockMvc.perform(post(INVENTORIES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createMainStorageInventoryWhenAmountIsInvalid() throws Exception {
        //GIVEN
        var request = inventory(UUID.randomUUID(), 0);
        given(inventoryService.create(request)).willReturn(new CreateInventoryRequest());

        //WHEN
        var response = mockMvc.perform(post(INVENTORIES_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(QUANTITY_MIN_IS_INVALID.getMessage());
        });
    }

    //endregion

}
