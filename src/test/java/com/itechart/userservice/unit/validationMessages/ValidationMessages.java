package com.itechart.userservice.unit.validationMessages;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ValidationMessages {

    VALUE_IS_NULL("must not be null"),
    NAME_IS_INVALID("must match \"[a-zA-Z]{2,}\""),
    EMAIL_IS_INVALID("must match \"^([a-zA-Z0-9_-]+\\.)*[a-zA-Z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$\""),
    PASSWORD_IS_INVALID("must match \"(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[0-9a-zA-Z]{8,}\""),
    QUANTITY_MIN_IS_INVALID("must be greater than or equal to 1"),
    QUANTITY_MAX_IS_INVALID("must be less than or equal to 10");

    private String message;

}
