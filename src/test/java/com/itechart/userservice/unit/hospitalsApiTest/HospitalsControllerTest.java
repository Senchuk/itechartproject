package com.itechart.userservice.unit.hospitalsApiTest;

import static com.itechart.userservice.unit.validationMessages.ValidationMessages.VALUE_IS_NULL;
import static com.itechart.userservice.utils.TestUtils.hospital;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import static org.mockito.BDDMockito.given;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.type.TypeReference;

import com.itechart.userservice.exception.UnexpectedError;
import com.itechart.userservice.generated.model.Hospital;
import com.itechart.userservice.hospital.service.HospitalService;
import com.itechart.userservice.unit.AbstractUnitTest;

import org.junit.Test;

import org.mockito.Mock;

public class HospitalsControllerTest extends AbstractUnitTest {

    private static final String HOSPITALS_URL = BASE_URL + "/hospitals";

    @Mock
    private HospitalService hospitalService;

    //region create patient tests

    @Test
    public void createHospital_happyPath() throws Exception {
        //GIVEN
        var request = hospital("test", "test", UUID.randomUUID());
        given(hospitalService.create(request)).willReturn(new Hospital());

        //WHEN
        var response = mockMvc.perform(post(HOSPITALS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }

    @Test
    public void createHospitalWithMultipleInvalidFields() throws Exception {
        //GIVEN
        var request = hospital(null, null, null);
        given(hospitalService.create(request)).willReturn(new Hospital());

        //WHEN
        var response = mockMvc.perform(post(HOSPITALS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getResponse().getStatus()).isEqualTo(400);
            softly.assertThat(errors.size()).isEqualTo(3);
        });
    }

    @Test
    public void createHospitalWithoutName() throws Exception {
        //GIVEN
        var request = hospital(null, "test", UUID.randomUUID());
        given(hospitalService.create(request)).willReturn(new Hospital());

        //WHEN
        var response = mockMvc.perform(post(HOSPITALS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createHospitalWithoutAddress() throws Exception {
        //GIVEN
        var request = hospital("test", null, UUID.randomUUID());
        given(hospitalService.create(request)).willReturn(new Hospital());

        //WHEN
        var response = mockMvc.perform(post(HOSPITALS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    @Test
    public void createHospitalWithoutUserId() throws Exception {
        //GIVEN
        var request = hospital("test", "test", null);
        given(hospitalService.create(request)).willReturn(new Hospital());

        //WHEN
        var response = mockMvc.perform(post(HOSPITALS_URL)
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        final List<UnexpectedError> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<List<UnexpectedError>>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo(VALUE_IS_NULL.getMessage());
        });
    }

    //endregion

}
